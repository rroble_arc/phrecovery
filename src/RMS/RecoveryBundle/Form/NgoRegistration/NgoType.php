<?php

namespace RMS\RecoveryBundle\Form\NgoRegistration;

use RMS\RecoveryBundle\Model\Enums;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of RegisterNgoType
 *
 * @author rluna
 */
class NgoType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('name', null, array(
                    'attr' => array('class' => 'form-control'),
                    'label' => 'Organization Name',
                ))
                ->add('logo', null, array(
                    'attr' => array('class' => 'form-control'),
                    'label' => 'Logo URL'
                ))
                ->add('description', null, array(
                    'attr' => array(
                        'class' => 'form-control',
                        'maxlength' => 200
                    ),
                    'label' => 'NGO Description',
                    'required' => true,
                ))
                ->add('city', null, array(
                    'attr' => array('class' => 'form-control'),
                    'required' => true,
                ))
                ->add('country', null, array(
                    'attr' => array('class' => 'form-control'),
                    'required' => true,
                ))
                ->add('website', null, array(
                    'attr' => array('class' => 'form-control'),
                    'label' => 'NGO Website'
                ))
                ->add('ngoType', 'choice', array(
                    'choices' => Enums::ngoTypes(),
                    'attr' => array('class' => 'form-control edit-ngotype'),
                    'label' => 'NGO type',
                ))
//                ->add('skills', 'choice', array(
//                    'choices' => Enums::ngoSkills(),
//                    'multiple' => true,
//                    'expanded' => true,
//                    'required' => false,
//                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RMS\RecoveryBundle\Entity\Ngo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rms_recoverybundle_ngo';
    }

}
