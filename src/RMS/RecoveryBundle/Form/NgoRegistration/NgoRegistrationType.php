<?php

namespace RMS\RecoveryBundle\Form\NgoRegistration;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of RegisterUserType
 *
 * @author rluna
 */
class NgoRegistrationType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $reg = $options['data'];
        $builder
                ->add('ngo', new NgoType(), array(
                    'data' => $reg->getNgo()
                ))
                ->add('user', new UserType(), array(
                    'data' => $reg->getUser()
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RMS\RecoveryBundle\Model\NgoRegistration',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rms_recoverybundle_user';
    }

}
