<?php

namespace RMS\RecoveryBundle\Form\NgoRegistration;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of RegisterUserType
 *
 * @author rluna
 */
class UserType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('firstname', null, array(
                    'attr' => array('class' => 'form-control')
                ))
                ->add('lastname', null, array(
                    'attr' => array('class' => 'form-control')
                ))
                ->add('email', null, array(
                    'attr' => array('class' => 'form-control')
                ))
                ->add('contactInfo', null, array(
                    'attr' => array('class' => 'form-control'),
                    'required' => true,
                ))
                ->add('username', null, array(
                    'attr' => array('class' => 'form-control')))
                ->add('password', 'repeated', array(
                    'type' => 'password',
                    'required' => true,
                    'first_options' => array(
                        'attr' => array('class' => 'form-control'),
                        'label' => 'Password',
                    ),
                    'second_options' => array(
                        'attr' => array('class' => 'form-control',
                            'style' => 'margin-bottom:20px;'
                        ),
                        'label' => 'Confirm password',
                    ),
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RMS\RecoveryBundle\Entity\UserNgo',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rms_recoverybundle_user';
    }

}
