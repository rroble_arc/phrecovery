<?php

namespace RMS\RecoveryBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProjectLocationType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('province', 'entity', array(
                    'class' => 'RMSRecoveryBundle:Province',
                    'attr' => array('class' => 'form-control edit-province'),
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                                ->orderBy('p.name', 'ASC');
                    },
                    'label' => 'Province *',
                ))
                ->add('name', 'text', array(
                    'label' => 'Name *',
                    'attr' => array('class' => 'form-control edit-location-name')
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RMS\RecoveryBundle\Entity\HealthLocation',
            'request' => null,
        ));
        $resolver->addAllowedTypes(array(
            'request' => 'Symfony\Component\HttpFoundation\Request',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rms_recoverybundle_health_location';
    }

}
