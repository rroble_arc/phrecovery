<?php

namespace RMS\RecoveryBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MedicalTeamType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('password', 'repeated', array(
                    'type' => 'password',
                    'required' => false,
                    'first_options' => array('label' => 'Password', 'attr' => array('class' => 'form-control', 'autocomplete' => 'off')),
                    'second_options' => array('label' => 'Confirm Password', 'attr' => array('class' => 'form-control', 'autocomplete' => 'off')),
                    'invalid_message' => 'Passwords do not match'
                ))
                ->add('firstname', 'text', array(
					'label' => 'First name *',
                    'attr' => array('class' => 'form-control')
                ))
                ->add('lastname', 'text', array(
					'label' => 'Last name *',
                    'attr' => array('class' => 'form-control')
                ))
                ->add('email', 'text', array(
					'label' => 'Email *',
                    'attr' => array('class' => 'form-control')
                ))
                ->add('title', 'text', array(
					'label' => 'Title *',
                    'attr' => array('class' => 'form-control')
                ))
                ->add('contactInfo', 'text', array(
					'label' => 'Contact Info',
                    'attr' => array('class' => 'form-control')
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RMS\RecoveryBundle\Entity\HealthTeam',
            'request' => null,
        ));
        $resolver->addAllowedTypes(array(
            'request' => 'Symfony\Component\HttpFoundation\Request',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rms_recoverybundle_medicalteam';
    }

}
