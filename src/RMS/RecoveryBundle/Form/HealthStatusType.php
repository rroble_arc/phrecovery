<?php

namespace RMS\RecoveryBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class HealthStatusType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('message', 'textarea', array(
					'label' => 'Message *',
                    'attr' => array('class' => 'form-control', 'rows'=> '4', 'maxlength' => '200' )
                ))
                ->add('location', null, array(
					'label' => 'Location *',
                    'attr' => array('class' => 'form-control'),
                    'query_builder' => function(EntityRepository $er) {
                        $qb = $er->createQueryBuilder('l')
                                ->leftJoin('l.province', 'p')
                                ->orderBy('p.name', 'ASC')
                                ->addOrderBy('l.name', 'ASC');
                        return $qb;
                    },
                    'property' => 'locationName',
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RMS\RecoveryBundle\Entity\HealthStatus',
            'request' => null,
        ));
        $resolver->addAllowedTypes(array(
            'request' => 'Symfony\Component\HttpFoundation\Request',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rms_recoverybundle_healthstatus';
    }

}
