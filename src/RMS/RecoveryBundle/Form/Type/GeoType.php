<?php

namespace RMS\RecoveryBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class GeoType extends EntityType
{

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'class' => 'RMSRecoveryBundle:Geo',
            'choices' => function(Options $option) {
                if ( isset($option['filter_barangay']) && $option['filter_barangay'])
                    return $this->getChoices(false);
                return $this->getChoices(true);
            },
            'property' => 'optionName',
            'filter_barangay' => false
        ));
    }
    
    private function getChoices($barangay=true)
    {
        $choices = array();
        $em = $this->registry->getManager();
        $provinces = $em->getRepository('RMSRecoveryBundle:Province')
                ->findBy(array(), array('name' => 'ASC'));
        foreach ($provinces as $p) {
            $choices[] = $p;
            foreach ($p->getMunicipalities() as $m) {
                $choices[] = $m;
                if ( $barangay )
                {
                    foreach ($m->getBarangays() as $b) {
                        $choices[] = $b;
                    }
                }
            }
        }
        return $choices;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'entity';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'geo';
    }

}
