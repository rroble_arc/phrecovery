<?php

namespace RMS\RecoveryBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class GeoBarangayType extends EntityType
{

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'class' => 'RMSRecoveryBundle:Barangay',
            'group_by' => 'municipality.name',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'entity';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'geo_barangay';
    }

}
