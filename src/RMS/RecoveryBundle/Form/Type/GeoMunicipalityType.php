<?php

namespace RMS\RecoveryBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class GeoMunicipalityType extends EntityType
{

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'class' => 'RMSRecoveryBundle:Municipality',
            'group_by' => 'province.name',
            'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('m')
                        ->leftJoin('m.province', 'p')
                        ->orderBy('p.name', 'ASC')
                        ->addOrderBy('m.name', 'ASC');
            }
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'entity';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'geo_municipality';
    }

}
