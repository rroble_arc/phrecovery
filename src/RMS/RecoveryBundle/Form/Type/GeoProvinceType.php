<?php

namespace RMS\RecoveryBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class GeoProvinceType extends EntityType
{

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'class' => 'RMSRecoveryBundle:Province',
            'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('p')
                        ->orderBy('p.name', 'ASC');
            }
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'entity';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'geo_province';
    }

}
