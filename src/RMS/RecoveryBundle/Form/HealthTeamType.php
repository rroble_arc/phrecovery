<?php

namespace RMS\RecoveryBundle\Form;

use Doctrine\ORM\EntityRepository;
use RMS\RecoveryBundle\Model\Enums;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class HealthTeamType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('name', 'text', array(
                    'label' => 'Name *',
                    'attr' => array('class' => 'form-control')
                ))
				->add('ngo', null, array(
					'label' => 'NGO *',
					'attr' => array('class' => 'form-control'),
					'empty_value' => false
				))
                ->add('location', null, array(
                    'label' => 'Location *',
                    'attr' => array('class' => 'form-control'),
                    'query_builder' => function(EntityRepository $er) {
                        $qb = $er->createQueryBuilder('l')
                                ->leftJoin('l.province', 'p')
                                ->orderBy('p.name', 'ASC')
                                ->addOrderBy('l.name', 'ASC');
                        return $qb;
                    },
                    'property' => 'locationName',
                ))
                ->add('md', 'text', array(
                    'label' => 'MD',
                    'attr' => array('class' => 'form-control')
                ))
                ->add('rn', 'text', array(
                    'label' => 'RN',
                    'attr' => array('class' => 'form-control')
                ))
                ->add('other', 'text', array(
                    'label' => 'Other',
                    'attr' => array('class' => 'form-control')
                ))
                /*                 ->add('skills', 'choice', array(
                  'label' => 'Skills',
                  'attr' => array('class' => 'form-control'),
                  'choices' => Enums::ngoSkills(),
                  'multiple' => true,
                  'required' => false,
                  )) */
                /*                 ->add('skills', 'text', array(
                  'label' => 'Skills',
                  'attr' => array('class' => 'form-control'),
                  'required' => false,
                  )) */
                ->add('servicesRendered', 'textarea', array(
                    'label' => 'Services Rendered',
                    'attr' => array('class' => 'form-control', 'rows' => '3', 'maxlength' => '150'),
                    'required' => false,
                ))
                ->add('dateArrived', null, array
                    ('format' => 'dd/MM/yyyy',
                    'widget' => 'single_text',
                    'attr' => array(
                        'class' => 'form-control datepicker',
                        'placeholder' => 'dd/MM/yyyy'
                    ),
                    'label' => 'Date Arrived *'
                ))
                ->add('expectedDeparture', null, array
                    ('format' => 'dd/MM/yyyy',
                    'widget' => 'single_text',
                    'attr' => array(
                        'class' => 'form-control datepicker',
                        'placeholder' => 'dd/MM/yyyy'
                    ),
                    'label' => 'Expected Departure'
                ))
                ->add('contactPerson', 'text', array(
                    'label' => 'Contact Person',
                    'attr' => array('class' => 'form-control')
                ))
                ->add('contactNumber', 'text', array(
                    'label' => 'Contact Number',
                    'attr' => array('class' => 'form-control')
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RMS\RecoveryBundle\Entity\HealthTeam',
            'request' => null,
        ));
        $resolver->addAllowedTypes(array(
            'request' => 'Symfony\Component\HttpFoundation\Request',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rms_recoverybundle_healthteam';
    }

}
