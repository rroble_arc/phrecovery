<?php

namespace RMS\RecoveryBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AidEntryType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $province = $municipality = null;

        $aid = $options['data'];
        if ($aid->getMunicipality()) {
            $municipality = $aid->getMunicipality()->getId();
        }
        if ($aid->getProvince()) {
            $province = $aid->getProvince()->getId();
        }

        $request = $options['request']->request->all();
        $data = isset($request[$this->getName()]) ? $request[$this->getName()] : array();
        if (isset($data['province'])) {
            $province = $data['province'];
        }
        if (isset($data['municipality'])) {
            $municipality = $data['municipality'];
        }

        $builder
                ->add('province', 'entity', array(
                    'class' => 'RMSRecoveryBundle:Province',
                    'attr' => array('class' => 'form-control edit-province'),
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                                ->orderBy('p.name', 'ASC');
                    },
                    'label' => 'Region',
                    'empty_value' => '',
                ))
                ->add('municipality', 'entity', array(
                    'class' => 'RMSRecoveryBundle:Municipality',
                    'attr' => array('class' => 'form-control edit-municipality'),
                    'label' => 'Municipality / City',
                    'query_builder' => function(EntityRepository $er) use($province) {
                        $qb = $er->createQueryBuilder('m')
                                ->orderBy('m.name', 'ASC');
                        if ($province) {
                            $qb->where($qb->expr()->eq('m.province', $province));
                        } else {
                            $qb->setMaxResults(0);
                        }
                        return $qb;
                    },
                    'empty_value' => '',
                ))
                ->add('geo', 'entity', array(
                    'class' => 'RMSRecoveryBundle:Barangay',
                    'attr' => array('class' => 'form-control edit-barangay'),
                    'label' => 'Barangay / Town',
                    'empty_value' => '',
                    'query_builder' => function(EntityRepository $er) use($municipality) {
                        $qb = $er->createQueryBuilder('b')
                                ->orderBy('b.name', 'ASC');
                        if ($municipality) {
                            $qb->where($qb->expr()->eq('b.municipality', $municipality));
                        } else {
                            $qb->setMaxResults(0);
                        }
                        return $qb;
                    },
                ))
                ->add('dateStart', null, array
                    ('format' => 'dd/MM/yyyy',
                    'widget' => 'single_text',
                    'attr' => array(
                        'class' => 'form-control datepicker',
                        'placeholder' => 'dd/MM/yyyy'
                    ),
                    'label' => 'Aid Start Date'
                ))
                ->add('type', null, array(
                    'label' => 'Type of Aid',
                    'attr' => array('class' => 'form-control'),
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('t')
                                ->orderBy('t.name', 'ASC');
                    },
                ))
                ->add('quantity', null, array(
                    'attr' => array('class' => 'form-control'),
                    'required' => true,
                ))
                ->add('description', 'text', array(
                    'attr' => array('class' => 'form-control')
                ))
                ->add('peopleCatered', null, array(
                    'label' => 'People Serviced',
                    'attr' => array('class' => 'form-control')
                ))
                ->add('duration', 'integer', array(
                    'label' => 'Supply Duration(days)',
                    'attr' => array('class' => 'form-control'),
                    'required' => false,
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RMS\RecoveryBundle\Entity\Aid',
            'request' => null,
        ));
        $resolver->addAllowedTypes(array(
            'request' => 'Symfony\Component\HttpFoundation\Request',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rms_recoverybundle_aid';
    }

}
