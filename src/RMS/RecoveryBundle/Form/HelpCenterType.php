<?php

namespace RMS\RecoveryBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 *  @author Jun Rama <j.rama@arcanys.com>
 */

class HelpCenterType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('contact', 'text', array('label' => 'Contact Person', 'attr' => array('class' => 'form-control')))
            ->add('geo', 'geo', array('label' => 'Location', 'filter_barangay' => true, 'attr' => array('class' => 'form-control')))
            ->add('type', null, array(
                'label' => 'Type of Resource Needed', 
                'attr' => array('class' => 'form-control'),
                'query_builder' => function(EntityRepository $er) {
                    $qb =  $er->createQueryBuilder('t')
                            ->orderBy('t.name', 'ASC');
                    return $qb;
                },
            ))
            ->add('message', null, array('label' => 'Description', 'attr' => array('class' => 'form-control')));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RMS\RecoveryBundle\Entity\HelpCenter'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rms_recoverybundle_helpcenter';
    }
}
