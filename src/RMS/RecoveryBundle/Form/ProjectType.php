<?php

namespace RMS\RecoveryBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProjectType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $province = $municipality = null;

        $project = $options['data'];
        if ($project->getMunicipality()) {
            $municipality = $project->getMunicipality()->getId();
        }
        if ($project->getProvince()) {
            $province = $project->getProvince()->getId();
        }

        $request = $options['request']->request->all();
        $data = isset($request[$this->getName()]) ? $request[$this->getName()] : array();
        if (isset($data['province'])) {
            $province = $data['province'];
        }
        if (isset($data['municipality'])) {
            $municipality = $data['municipality'];
        }

        $builder
                ->add('province', 'entity', array(
                    'class' => 'RMSRecoveryBundle:Province',
                    'attr' => array('class' => 'form-control edit-province-project'),
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                                ->orderBy('p.name', 'ASC');
                    },
                    'label' => 'Region',
					'empty_value' => '',
                ))
                ->add('municipality', 'entity', array(
                    'class' => 'RMSRecoveryBundle:Municipality',
                    'attr' => array('class' => 'form-control edit-municipality-project'),
                    'label' => 'Municipality / City',
                    'query_builder' => function(EntityRepository $er) use($province) {
                        $qb = $er->createQueryBuilder('m')
                                ->orderBy('m.name', 'ASC');
                        if ($province) {
                            $qb->where($qb->expr()->eq('m.province', $province));
                        } else {
                            $qb->setMaxResults(0);
                        }
                        return $qb;
                    },
                    'empty_value' => '',
                ))
                ->add('geo', 'entity', array(
                    'class' => 'RMSRecoveryBundle:Barangay',
                    'attr' => array('class' => 'form-control edit-barangay-project'),
                    'label' => 'Barangay / Town',
                    'empty_value' => '',
                    'query_builder' => function(EntityRepository $er) use($municipality) {
                        $qb = $er->createQueryBuilder('b')
                                ->orderBy('b.name', 'ASC');
                        if ($municipality) {
                            $qb->where($qb->expr()->eq('b.municipality', $municipality));
                        } else {
                            $qb->setMaxResults(0);
                        }
                        return $qb;
                    },
                ))
                ->add('description', 'textarea', array(
                    'attr' => array('class' => 'form-control', 'rows' => '5', 'maxlength' => '400'),
                ))
                ->add('dateStart', null, array
                    ('format' => 'dd/MM/yyyy',
                    'widget' => 'single_text',
                    'attr' => array(
                        'class' => 'form-control datepicker',
                        'placeholder' => 'dd/MM/yyyy'
                    ),
                    'label' => 'Date Start'
                ))
                ->add('dateEnd', null, array
                    ('format' => 'dd/MM/yyyy',
                    'widget' => 'single_text',
                    'attr' => array(
                        'class' => 'form-control datepicker',
                        'placeholder' => 'dd/MM/yyyy'
                    ),
                    'label' => 'Date End'
                ))
                ->add('peopleRequired', null, array(
                    'attr' => array('class' => 'form-control')
                ))
                /*->add('skills', 'textarea', array(
                    'attr' => array('class' => 'form-control', 'rows' => '5', 'maxlength' => '100'),
                ))*/
                ->add('type', null, array(
                    'label' => 'Project Type',
                    'attr' => array('class' => 'form-control'),
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('t')
                                ->orderBy('t.name', 'ASC');
                    },
                ))
                ->add('contactInstructions', 'textarea', array(
                    'attr' => array('class' => 'form-control', 'rows' => '5', 'maxlength' => '200'),
                ))
                ->add('contactPerson', 'text', array(
                    'label' => 'Contact Person',
                    'attr' => array('class' => 'form-control')
                ))
                ->add('contactNumber', 'text', array(
                    'label' => 'Contact Number',
                    'attr' => array('class' => 'form-control'),
					'required' => false,
                ))
                ->add('email', null, array(
                    'attr' => array('class' => 'form-control'),
					'required' => false,
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RMS\RecoveryBundle\Entity\Project',
            'request' => null,
        ));
        $resolver->addAllowedTypes(array(
            'request' => 'Symfony\Component\HttpFoundation\Request',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rms_recoverybundle_project';
    }

}
