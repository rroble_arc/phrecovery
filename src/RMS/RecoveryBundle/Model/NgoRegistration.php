<?php

namespace RMS\RecoveryBundle\Model;

use RMS\RecoveryBundle\Entity\Ngo;
use RMS\RecoveryBundle\Entity\UserNgo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Description of NgoRegistration
 *
 * @author rluna
 */
class NgoRegistration
{

    /**
     * @Assert\Valid()
     * 
     * @var Ngo
     */
    protected $ngo;

    /**
     * @Assert\Valid()
     * 
     * @var UserNgo
     */
    protected $user;
    
    public function __construct()
    {
        $this->ngo = new Ngo();
        $this->user = new UserNgo();
    }

    /**
     * @return Ngo
     */
    public function getNgo()
    {
        return $this->ngo;
    }

    public function setNgo(Ngo $ngo)
    {
        $this->ngo = $ngo;
    }

    /**
     * @return UserNgo
     */
    public function getUser()
    {
        return $this->user;
    }

    public function setUser(UserNgo $user)
    {
        $this->user = $user;
    }

}
