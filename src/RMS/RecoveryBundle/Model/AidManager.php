<?php

namespace RMS\RecoveryBundle\Model;

use RMS\RecoveryBundle\Entity\Geo;
use Symfony\Bridge\Doctrine\ManagerRegistry;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class AidManager
{

    /**
     * @var 
     */
    protected $em;
    
    /**
     * @var array
     */
    protected $geoIds;

    public function __construct(ManagerRegistry $registry)
    {
        $this->em = $registry->getManager();
    }

    public function hasActivities(Geo $geo)
    {
        $this->init();
        return $geo->hasActivities($this);
    }
    
    public function hasAids(Geo $geo)
    {
        return array_key_exists($geo->getId(), $this->geoIds);
    }
    
    protected function init()
    {
        if ($this->geoIds === null) {
            $this->geoIds = array();
            $result = $this->findAllGeoIds();
            foreach ($result as $aid) {
                $aidId = $aid['id'];
                $this->geoIds[$aidId] = $aidId;
            }
        }
    }
 
    /**
     * @return array
     */
    public function findAllGeoIds()
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('g.id')->distinct(true);
        $qb->from('RMSRecoveryBundle:Aid', 'a')
                ->leftJoin('a.geo', 'g');
        $result = $qb->getQuery()->getScalarResult();
        return $result;
    }
    
}
