<?php

namespace RMS\RecoveryBundle\Model;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
abstract class Enums
{

    const NGO_TYPE_HEALTH = 'health',
            NGO_TYPE_PRIVATE = 'private';

    public static function ngoTypes()
    {
        return array(
            static::NGO_TYPE_HEALTH => 'Health NGO',
            static::NGO_TYPE_PRIVATE => 'Non-Health NGO, Private Relief Group or Company',
        );
    }

    public static function validNgoTypes()
    {
        return array_keys(static::ngoTypes());
    }

    public static function ngoSkills()
    {
        $array = array('FMT1', 'FMT2', 'FMT3', 'WASH', 'Health', 'MHPSS', 'NUT');
        return array_combine($array, $array);
    }
}
