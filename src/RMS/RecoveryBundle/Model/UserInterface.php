<?php

namespace RMS\RecoveryBundle\Model;

use Symfony\Component\Security\Core\User\UserInterface as BaseUserInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
interface UserInterface extends BaseUserInterface
{
}
