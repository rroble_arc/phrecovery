<?php

namespace RMS\RecoveryBundle\Model;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class CsvReader
{

    protected $file;
    
    protected $ignoreLine;
    
    protected $ignoreEmpty;

    public function __construct($file, $ignoreLine = null, $ignoreEmpty = true)
    {
        $this->file = $file;
        $this->ignoreLine = $ignoreLine;
        $this->ignoreEmpty = $ignoreEmpty;
    }
    
    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file)
    {
        $this->file = $file;
    }

    public function getIgnoreLine()
    {
        return $this->ignoreLine;
    }

    public function setIgnoreLine($ignoreLine)
    {
        $this->ignoreLine = $ignoreLine;
    }

    public function getIgnoreEmpty()
    {
        return $this->ignoreEmpty;
    }

    public function setIgnoreEmpty($ignoreEmpty)
    {
        $this->ignoreEmpty = $ignoreEmpty;
    }
    
    public function read(\Closure $rowCallback = null)
    {
        $rows = array();
        if (($handle = fopen($this->file, "r")) !== FALSE) {
            $i = 0;
            while (($data = fgetcsv($handle)) !== FALSE) {
                $i++;
                if ($this->ignoreLine && $i <= $this->ignoreLine) {
                    continue;
                }
                if ($this->ignoreEmpty && trim(implode($data)) == '') {
                    continue;
                }
                if ($rowCallback) {
                    $rows[] = array_map($rowCallback, $data);
                } else {
                    $rows[] = array_map('trim', $data);
                }
            }
            fclose($handle);
        }
        return $rows;
    }

}
