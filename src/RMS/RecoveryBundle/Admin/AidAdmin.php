<?php

namespace RMS\RecoveryBundle\Admin;

use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class AidAdmin extends Admin
{

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->leftJoin($query->getRootAlias().'.ngo', 'ngo');
        $query->leftJoin($query->getRootAlias().'.geo', 'geo');
        $query->leftJoin($query->getRootAlias().'.type', 'type');
        return $query;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('geo', 'geo',  array('label' => 'Location'))
                ->add('ngo', null, array('label' => 'NGO',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('n')
                                ->orderBy('n.name', 'ASC');
                    }))
                ->add('type', 'sonata_type_model', array(
                    'query' => $this->modelManager
                    ->createQuery('RMSRecoveryBundle:AidType', 't')
                    ->orderBy('t.name', 'ASC')
                ))
                ->add('description', null, array('attr' => array('maxlength' => 150)))
                ->add('quantity')
                ->add('peopleCatered')
                ->add('dateStart', null, array(
                    'attr' => array('class' => 'datepicker', 'placeholder' => 'dd/mm/yyyy'),
                    'format' => 'dd/MM/yyyy',
                    'widget' => 'single_text',
                ))
                ->add('dateEnd', null, array(
                    'attr' => array('class' => 'datepicker', 'placeholder' => 'dd/mm/yyyy'),
                    'format' => 'dd/MM/yyyy',
                    'widget' => 'single_text',
                ))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('geo', null,  array('label' => 'Location'))
                ->add('ngo', null, array('label' => 'NGO'))
                ->add('type')
                ->add('description')
                ->add('quantity')
                ->add('peopleCatered')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('description')
                ->add('geo.name', null,  array('label' => 'Location'))
                ->add('ngo.name', null, array('label' => 'NGO'))
                ->add('type.name', null, array('label' => 'Aid Type'))
                ->add('quantity')
                ->add('peopleCatered')
                ->add('dateStart')
                ->add('dateEnd')
        ;
    }

}
