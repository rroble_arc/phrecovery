<?php

namespace RMS\RecoveryBundle\Admin;

use Doctrine\ORM\EntityRepository;
use RMS\RecoveryBundle\Model\Enums;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @author Dottie Decano <d.decano@arcanys.com>
 */
class ProjectAdmin extends Admin
{
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->leftJoin($query->getRootAlias().'.ngo', 'ngo');
		$query->leftJoin($query->getRootAlias().'.geo', 'geo');
        return $query;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('ngo', null, array('label' => 'NGO',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('n')
                                ->orderBy('n.name', 'ASC');
                    }))
                ->add('description', 'textarea', array(
                    'attr' => array('rows' => '5', 'maxlength' => '400'),
                    'required' => false,
                ))
                ->add('dateStart', null, array(
                    'attr' => array('class' => 'datepicker', 'placeholder' => 'dd/mm/yyyy'),
                    'format' => 'dd/MM/yyyy',
                    'widget' => 'single_text',
                ))
                ->add('dateEnd', null, array(
                    'attr' => array('class' => 'datepicker', 'placeholder' => 'dd/mm/yyyy'),
                    'format' => 'dd/MM/yyyy',
                    'widget' => 'single_text',
                ))
                ->add('peopleRequired')
                /*->add('skills', 'textarea', array(
                    'label' => 'Skills',
                    'attr' => array('rows' => '4', 'maxlength' => '100'),
                    'required' => false,
                ))*/
                ->add('type', 'sonata_type_model', array(
                    'query' => $this->modelManager
                    ->createQuery('RMSRecoveryBundle:ProjectType', 't')
                    ->orderBy('t.name', 'ASC')
                ))
                ->add('contactInstructions', 'textarea', array(
                    'label' => 'Contact Instructions',
                    'attr' => array('rows' => '4', 'maxlength' => '200'),
                    'required' => false,
                ))
                ->add('contactPerson')
                ->add('contactNumber')
                ->add('email')
				->add('geo', 'geo',  array('label' => 'Project Location'))
				->add('map', 'hidden', array('attr' => array('class' => 'edit-map')))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
				->add('ngo', null, array('label' => 'NGO'))
                ->add('geo', null,  array('label' => 'Location'))
                ->add('description')
				->add('type')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
				->add('ngo.name', null, array('label' => 'NGO'))
                ->add('geo.name', null,  array('label' => 'Location'))
                ->add('description')
                ->add('peopleRequired')
                //->add('skills')
				->add('type.name', null, array('label' => 'Project Type'))
                ->add('contactPerson')
                ->add('contactNumber')
        ;
    }

}
