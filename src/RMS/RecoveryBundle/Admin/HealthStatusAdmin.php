<?php

namespace RMS\RecoveryBundle\Admin;

use Doctrine\ORM\EntityRepository;
use RMS\RecoveryBundle\Model\Enums;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class HealthStatusAdmin extends Admin
{
    
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->leftJoin($query->getRootAlias().'.location', 'location');
        $query->leftJoin($query->getRootAlias().'.reporter', 'reporter');
        return $query;
    }
    
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('location', 'sonata_type_model', array(
                    'query' => $this->modelManager
                        ->createQuery('RMSRecoveryBundle:HealthLocation', 'l')
                        ->leftJoin('l.province', 'p')
                        ->orderBy('l.name', 'ASC'),
                ))
                ->add('reporter', null, array('label' => 'NGO',
                    'query_builder' => function(EntityRepository $er) {
                        $qb = $er->createQueryBuilder('n')->orderBy('n.name', 'ASC');
                        $health = $qb->expr()->literal(Enums::NGO_TYPE_HEALTH);
                        $qb->where($qb->expr()->eq('n.ngoType', $health));
                        return $qb;
                    }))
                ->add('message')
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('location')
                ->add('reporter')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('message')
                ->add('location.name')
                ->add('reporter.name')
        ;
    }

}
