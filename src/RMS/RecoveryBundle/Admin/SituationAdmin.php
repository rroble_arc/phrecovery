<?php

namespace RMS\RecoveryBundle\Admin;

use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class SituationAdmin extends Admin
{

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->leftJoin($query->getRootAlias() . '.geo', 'geo');
        $query->leftJoin($query->getRootAlias() . '.status', 'status');
        $query->leftJoin($query->getRootAlias() . '.reporter', 'reporter');
        return $query;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('reporter', null, array(
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                                ->orderBy('u.firstname', 'ASC');
                    }))
                ->add('geo', 'geo', array('label' => 'Location'))
                ->add('status', 'sonata_type_model', array(
                    'query' => $this->modelManager
                    ->createQuery('RMSRecoveryBundle:Status', 's')
                    ->orderBy('s.description', 'ASC')
                ))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('geo', null, array('label' => 'Location'))
                ->add('reporter')
                ->add('status')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('geo.name', null, array('label' => 'Location'))
                ->add('status.description', null, array('label' => 'Status'))
                ->add('reporter.firstname', null, array('label' => 'Reported by'))
        ;
    }

}
