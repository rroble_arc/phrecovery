<?php

namespace RMS\RecoveryBundle\Admin;

use RMS\RecoveryBundle\Model\Enums;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class HealthNGOAdmin extends NgoAdmin
{
    
    protected $baseRouteName = 'health_ngo';
    protected $baseRoutePattern = 'health-ngo';

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $health = $query->expr()->literal(Enums::NGO_TYPE_HEALTH);
        $query->where($query->expr()->eq($query->getRootAlias().'.ngoType', $health));
        return $query;
    }

}
