<?php

namespace RMS\RecoveryBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class BarangayAdmin extends Admin
{

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->leftJoin($query->getRootAlias().'.municipality', 'municipality');
        $query->leftJoin('municipality.province', 'province');
        return $query;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('map', 'hidden', array('attr' => array('class' => 'edit-map')))
                ->add('name')
                ->add('municipality', 'geo_municipality')
                ->add('population')
                ->add('estimatedHouseholds')
                ->add('affectedHouseholds')
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('name')
                ->add('municipality')
                ->add('municipality.province', null, array('label' => 'Province'))
                ->add('population')
                ->add('estimatedHouseholds')
                ->add('affectedHouseholds')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('name')
                ->add('municipality.name', null, array('label' => 'Municipality'))
                ->add('municipality.province.name', null, array('label' => 'Province'))
                ->add('population')
                ->add('estimatedHouseholds')
                ->add('affectedHouseholds')
        ;
    }

    public function getExportFields()
    {
        $fields = parent::getExportFields();
        $fields[] = 'municipality.province.name';
        $fields[] = 'municipality.name';
        return $fields;
    }

}
