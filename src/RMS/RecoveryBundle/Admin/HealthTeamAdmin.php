<?php

namespace RMS\RecoveryBundle\Admin;

use Doctrine\ORM\EntityRepository;
use RMS\RecoveryBundle\Model\Enums;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class HealthTeamAdmin extends Admin
{

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->leftJoin($query->getRootAlias().'.location', 'location');
        $query->leftJoin($query->getRootAlias().'.ngo', 'ngo');
        return $query;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('name')
                ->add('location', 'sonata_type_model', array(
                    'query' => $this->modelManager
                        ->createQuery('RMSRecoveryBundle:HealthLocation', 'l')
                        ->leftJoin('l.province', 'p')
                        ->orderBy('l.name', 'ASC'),
                ))
                ->add('ngo', null, array('label' => 'NGO',
                    'query_builder' => function(EntityRepository $er) {
                        $qb = $er->createQueryBuilder('n')->orderBy('n.name', 'ASC');
                        $health = $qb->expr()->literal(Enums::NGO_TYPE_HEALTH);
                        $qb->where($qb->expr()->eq('n.ngoType', $health));
                        return $qb;
                    }))
                ->add('md', null, array('label' => 'MD'))
				->add('rn', null, array('label' => 'RN'))
                ->add('other')
                //->add('skills')
                ->add('servicesRendered', 'textarea', array(
                    'label' => 'Services Rendered',
                    'attr' => array('rows' => '4', 'maxlength' => '150'),
                    'required' => false,
                ))
                ->add('dateArrived')
                ->add('expectedDeparture')
                ->add('contactPerson')
                ->add('contactNumber')
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('location')
                ->add('name')
                ->add('ngo')
                ->add('md')
                ->add('rn')
                ->add('other')
                //->add('skills')
                ->add('servicesRendered')
                ->add('contactPerson')
                ->add('contactNumber')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('name')
                ->add('location.name')
                ->add('ngo.name')
                ->add('md')
                ->add('rn')
                ->add('other')
                ->add('servicesRendered')
                ->add('contactPerson')
                ->add('contactNumber')
        ;
    }

}
