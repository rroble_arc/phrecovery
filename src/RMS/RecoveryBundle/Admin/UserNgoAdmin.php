<?php

namespace RMS\RecoveryBundle\Admin;

use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class UserNgoAdmin extends Admin
{
    
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->leftJoin($query->getRootAlias().'.ngo', 'ngo');
        return $query;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('username')
                ->add('password', 'repeated', array(
                    'type' => 'password',
                    'required' => false,
                    'first_options' => array('label' => 'Password', 'attr' => array('class' => 'span5', 'autocomplete' => 'off')),
                    'second_options' => array('label' => 'Confirm Password', 'attr' => array('class' => 'span5', 'autocomplete' => 'off')),
                ))
                ->add('firstname')
                ->add('lastname')
                ->add('email')
                ->add('ngo', null, array(
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('n')
                                ->orderBy('n.name', 'ASC');
                    }))
                ->add('title')
                ->add('contactInfo')
                ->add('coordinator', 'choice', array(
                    'label' => 'Is Coordinator',
                    'choices' => array('No', 'Yes')
                ))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('username')
                ->add('firstname')
                ->add('lastname')
                ->add('email')
                ->add('ngo', null, array('label' => 'NGO'))
                ->add('title')
                ->add('contactInfo')
                ->add('coordinator')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('username')
                ->add('firstname')
                ->add('lastname')
                ->add('email')
                ->add('ngo.name', null, array('label' => 'NGO'))
                ->add('title')
                ->add('contactInfo')
        ;
    }

}
