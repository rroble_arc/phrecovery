<?php

namespace RMS\RecoveryBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class AidNeededAdmin extends Admin
{
    
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->leftJoin($query->getRootAlias().'.geo', 'geo');
        return $query;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('geo', 'geo', array('label' => 'Location'))
                ->add('description')
//            ->add('status')
                ->add('aidTypes', null, array(
                    'required' => false,
                    'multiple' => true,
                ))
                ->add('reporter')
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('geo', null,  array('label' => 'Location'))
                ->add('description')
                ->add('aidTypes')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('geo.name', null,  array('label' => 'Location'))
                ->add('description')
                ->add('aidTypes')
        ;
    }

}
