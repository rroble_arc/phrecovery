<?php

namespace RMS\RecoveryBundle\Admin;

use Doctrine\ORM\EntityRepository;
use RMS\RecoveryBundle\Model\Enums;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class NgoAdmin extends Admin
{

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('name')
                ->add('logo')
                ->add('description', null, array('attr' => array('maxlength' => 200)))
                ->add('city')
                ->add('country')
                ->add('website')
                ->add('status', 'choice', array(
                    'label' => 'Verified status',
                    'choices' => array('No', 'Yes')
                ))
                ->add('ngoType', 'choice', array(
                    'choices' => Enums::ngoTypes(),
                ))
                /*->add('skills', 'choice', array(
                    'choices' => Enums::ngoSkills(),
                    'multiple' => true,
                    'required' => false,
                ))*/
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('name')
                ->add('description')
                ->add('city')
                ->add('country')
                ->add('status')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('name')
                ->add('ngoType')
                ->add('city')
                ->add('country')
                ->add('status', null, array('label' => 'Verified'))
        ;
    }

}
