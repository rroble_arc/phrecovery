<?php

namespace RMS\RecoveryBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class MunicipalityAdmin extends Admin
{
    
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->leftJoin($query->getRootAlias().'.province', 'province');
        return $query;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('map', 'hidden', array('attr' => array('class' => 'edit-map')))
                ->add('name')
                ->add('province', 'geo_province')
                ->add('population')
                ->add('estimatedHouseholds')
                ->add('affectedHouseholds')
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('name')
                ->add('province')
                ->add('population')
                ->add('estimatedHouseholds')
                ->add('affectedHouseholds')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('name')
                ->add('province.name', null, array('label' => 'Province'))
                ->add('population')
                ->add('estimatedHouseholds')
                ->add('affectedHouseholds')
        ;
    }
    
    public function getExportFields()
    {
        $fields = parent::getExportFields();
        $fields[] = 'province.name';
        return $fields;
    }

}
