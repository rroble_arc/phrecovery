<?php

namespace RMS\RecoveryBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class UserAdminAdmin extends Admin
{

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('username')
                ->add('password', 'repeated', array(
                    'type' => 'password',
                    'required' => false,
                    'first_options' => array('label' => 'Password', 'attr' => array('class' => 'span5', 'autocomplete' => 'off')),
                    'second_options' => array('label' => 'Confirm Password', 'attr' => array('class' => 'span5', 'autocomplete' => 'off')),
                ))
                ->add('firstname')
                ->add('lastname')
                ->add('email')
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('username')
                ->add('firstname')
                ->add('lastname')
                ->add('email')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id')
                ->add('username')
                ->add('firstname')
                ->add('lastname')
                ->add('email')
        ;
    }

}
