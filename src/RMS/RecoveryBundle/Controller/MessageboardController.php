<?php

namespace RMS\RecoveryBundle\Controller;

use RMS\RecoveryBundle\Entity\MessageBoard;
use RMS\RecoveryBundle\Entity\UserNgo;
use RMS\RecoveryBundle\Form\MessageBoardType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 * @Route("/messageboard")
 */
class MessageboardController extends Controller
{

    /**
     * @Route("/{id}", name="messageboard")
     * @Template()
     */
    public function indexAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $barangay = $em->getRepository('RMSRecoveryBundle:Barangay')->find($id);
        if (!$barangay) {
            return $this->createNotFoundException("Barangay <$id> does not exists.");
        }
        $user = $this->getUser();
        if ($user instanceof UserNgo) {
            $message = new MessageBoard();
            $message->setReporter($user);
            $message->setGeo($barangay);

            $form = $this->createForm(new MessageBoardType(), $message);

            if ($request->isMethod('post')) {
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $em->persist($message);
                    $em->flush();
                    
                    return $this->redirect($this->generateUrl('messageboard', array('id' => $id)));
                }
            }
            
            return array(
                'barangay' => $barangay,
                'form' => $form->createView(),
            );
        }

        return array('barangay' => $barangay);
    }

}
