<?php

namespace RMS\RecoveryBundle\Controller;

use RMS\RecoveryBundle\Entity\HealthStatus;
use RMS\RecoveryBundle\Form\PostHealthStatusType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Dottie Decano <d.decano@arcanys.com>
 * @Route("/posthealthstatus")
 */
class PostHealthStatusController extends Controller
{

    /**
     * @Route("/{id}", name="posthealthstatus")
     * @Template()
     */
    public function indexAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $location = $em->getRepository('RMSRecoveryBundle:HealthLocation')->find($id);
        if (!$location) {
            return $this->createNotFoundException("Location <$id> does not exists.");
        }

        $hs = $em->getRepository('RMSRecoveryBundle:HealthStatus')->createQueryBuilder('s')
            ->where(' s.location = :location ')
            ->setParameter('location', $id);
		;
        $list = $hs->select('s')
                ->leftJoin('s.location', 'l')
                ->leftJoin('l.province', 'p')
                ->orderBy('l.name', 'ASC')
                ->addOrderBy('s.dateCreated', 'DESC')
                ->getQuery()
                ->getResult();
        $locations_msg = array();
        foreach ($list as $status) {
			$name = $status->getLocation()->getName();
            $location = $status->getLocation();
            $id = $location->getId();
            if (!isset($locations_msg[$name])) {
                $locations_msg[$name] = array(
                    'location' => $location,
                    'messages' => array(),
                );
            }
            $locations_msg[$name]['messages'][] = $status;
        }

        $user = $this->getUser();
		$status = new HealthStatus();
		$status->setReporter($user->getNgo());
		$status->setLocation($location);
		$form = $this->createForm(new PostHealthStatusType(), $status, array('request' => $request));

		if ($request->isMethod('post')) {
			$form->handleRequest($request);
			if ($form->isValid()) {
				$em->persist($status);
				$em->flush();

				return $this->redirect($this->generateUrl('posthealthstatus', array('id' => $id)));
			}
		}
		return array(
			'location' => $location,
			'id' => $id,
			'locations_msg' => $locations_msg,
			'form' => $form->createView(),
		);

        return array('location' => $location);
    }

}
