<?php

namespace RMS\RecoveryBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class NgoListController extends Controller
{
    
    /**
     * @Route("/listing/ngo", name="ngo_list")
     * @Template()
     */
    public function listAction(Request $request, $max = 200, $partial = false)
    {
        $search = $request->get('q');
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('RMSRecoveryBundle:Ngo')->createQueryBuilder('n')
                ->orderBy('n.name');
        if ($search) {
            $str = $qb->expr()->literal("%{$search}%");
            $qb->where($qb->expr()->like('n.name', $str));
        }
        $ngos = $qb->setMaxResults($max)->getQuery()->getResult();
        $count = count($ngos);
        $div = 3;
        if ($count < 30) {
            $div = 2;
        } else if ($count < 20) {
            $div = 1;
        }
        $perColumn = ceil($count/$div);
        if ($partial || $request->isXmlHttpRequest()) {
            return $this->render('RMSRecoveryBundle:NgoList:listContent.html.twig', array(
                'ngos' => $ngos,
                'search' => $search,
                'per_column' => intval($perColumn),
            ));
        }
        return array(
            'ngos' => $ngos,
            'search' => $search,
            'per_column' => intval($perColumn),
        );
    }
    
}
