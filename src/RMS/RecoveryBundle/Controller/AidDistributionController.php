<?php

namespace RMS\RecoveryBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 * @Route("/aid/distribution")
 */
class AidDistributionController extends Controller
{

    /**
     * @Route("/{provinceId}", name="aid_distribution", defaults={"provinceId":null})
     * @Template()
     */
    public function indexAction($provinceId = null)
    {
        $em = $this->getDoctrine()->getManager();
        if ($provinceId) {
            $province = $em->getRepository('RMSRecoveryBundle:Province')
                    ->find($provinceId);
        } else {
            $province = null;
        }
        $provinces = $em->getRepository('RMSRecoveryBundle:Province')
                ->findBy(array(), array('name' => 'ASC'));
        return array(
            'provinces' => $provinces,
            'province' => $province,
            'checked_provinces' => array($provinceId),
            'checked_municipalities' => $this->getToggled('municipalities'),
            'checked_barangays' => $this->getToggled('barangays'),
        );
    }

    private function getToggled($name)
    {
        $toggled = array_filter($this->getData($name), function($v) {
            return $v === true;
        });
        return array_keys($toggled);
    }

    /**
     * @Route("/municipality/{id}", name="aid_distribution_municipality")
     * @Template()
     */
    public function municipalityDistributionAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('RMSRecoveryBundle:Municipality')->find($id);
        if (!$entity) {
            return $this->createNotFoundException("Municipality <$id> does not exists.");
        }
        return array(
            'municipality' => $entity,
            'collapse_municipality' => true,
            'checked_barangays' => $this->getToggled('barangays'),
        );
    }

    /**
     * @Route("/barangay/{id}", name="aid_distribution_barangay")
     * @Template()
     */
    public function barangayDistributionAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('RMSRecoveryBundle:Barangay')->find($id);
        if (!$entity) {
            return $this->createNotFoundException("Barangay <$id> does not exists.");
        }
        return array(
            'barangay' => $entity,
            'collapse_barangay' => true,
        );
    }

    private function getAttribute($name, $default = array())
    {
        $session = $this->getRequest()->getSession();
        if ($session->has($key = 'attr_'.$name)) {
            return $session->get($key);
        }
        return $this->setAttribute($name, $default);
    }

    private function setAttribute($name, $value)
    {
        $session = $this->getRequest()->getSession();
        $session->set('attr_'.$name, $value);
        return $value;
    }

    private function getData($attr, $key = null, $default = null)
    {
        $attrib = $this->getAttribute($attr);
        if ($key) {
            if (isset($attrib[$key])) {
                return $attrib[$key];
            }
            return $default;
        }
        return $attrib;
    }

    private function setData($attr, $key, $value)
    {
        $attrib = $this->getAttribute($attr);
        $attrib[$key] = $value;
        $this->setAttribute($attr, $attrib);
    }

    /**
     * @Route("/toggle/province", name="toggle_province")
     * @Template()
     */
    public function toggleProvinceAction(Request $request)
    {
        $id = $request->get('id');
        $toggle = (bool) $request->get('toggle');
        // do not persist checked_provinces
        if ($toggle) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('RMSRecoveryBundle:Province')->find($id);
            return array(
                'province' => $entity,
                'checked_municipalities' => $this->getToggled('municipalities'),
                'checked_barangays' => $this->getToggled('barangays'),
            );
        }
        return array();
    }

    /**
     * @Route("/toggle/municipality", name="toggle_municipality")
     * @Template()
     */
    public function toggleMunicipalityAction(Request $request)
    {
        $id = $request->get('id');
        $toggle = (bool) $request->get('toggle');
        $this->setData('municipalities', $id, $toggle);
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('RMSRecoveryBundle:Municipality')->find($id);
        return array(
            'municipality' => $entity,
            'collapse_municipality' => true,
            'checked_barangays' => $this->getToggled('barangays'),
            'toggle' => $toggle,
        );
    }

    /**
     * @Route("/toggle/barangay", name="toggle_barangay")
     * @Template()
     */
    public function toggleBarangayAction(Request $request)
    {
        $id = $request->get('id');
        $toggle = (bool) $request->get('toggle');
        $this->setData('barangays', $id, $toggle);
        if ($toggle) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('RMSRecoveryBundle:Barangay')->find($id);
            return array(
                'barangay' => $entity,
                'collapse_barangay' => true,
            );
        }
        return array();
    }

    /**
     * @Route("/summary/{id}", name="load_summary", defaults={"id":null})
     * @Template()
     */
    public function loadSummaryAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('RMSRecoveryBundle:Municipality')->find($id);
        if (!$entity) {
            return $this->createNotFoundException("Municipality <$id> does not exists.");
        }
        return $this->render('RMSRecoveryBundle:AidDistribution:_summaryContent.html.twig', array(
            'municipality' => $entity,
        ));
    }
}
