<?php

namespace RMS\RecoveryBundle\Controller;

use RMS\RecoveryBundle\Entity\UserNgo;
use RMS\RecoveryBundle\Form\NgoUserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Dottie Decano <d.decano@arcanys.com>
 * @Route("/ngo/user")
 * @method \RMS\RecoveryBundle\Entity\UserNgo getUser()
 */
class NgoUserController extends Controller
{
    /**
     * @Route("/list", name="user_list")
     * @Template()
     */
    public function listAction()
    {
		$user = $this->getUser();
		$em = $this->getDoctrine()->getManager();
		$users = $em->getRepository('RMSRecoveryBundle:UserNgo')
					->findBy(array('ngo' => $user->getNgo()->getId()));

        return array(
            'users' => $users,
        );
    }

    /**
     * @Route("/{id}/edit", name="edit_ngo_user")
     * @Template()
     */
    public function editNgoUserAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('RMSRecoveryBundle:UserNgo')->find($id);
        
        if (!$user)
        {
            $this->get('session')->getFlashBag()->add(
                    'notice', "Ngo User $id does not exist."
            );
            return $this->redirect($this->generateUrl('user_list'));
        }

        $form = $this->createForm(new NgoUserType(), $user, array('request' => $request));
		//var_dump($form);

        if ($request->isMethod('post'))
        {
            $form->handleRequest($request);
            if ($form->isValid())
            {
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                $this->get('session')->getFlashBag()->add(
                        'success', "User Successfully Saved!."
                );
                return $this->redirect($this->generateUrl('user_list'));
            }
			//var_dump($form->getErrorsAsString());
        }

        return array(
            'user' => $user,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/success", name="ngo_user_success")
     * @Template()
     */
    public function successAction()
    {
        return array();
    }	
}
