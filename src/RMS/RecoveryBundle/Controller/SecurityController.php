<?php

namespace RMS\RecoveryBundle\Controller;

use RMS\RecoveryBundle\Entity\UserAdmin;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class SecurityController extends Controller
{
    
    /**
     * @Route("/login", name="login")
     * @Template()
     */
    public function loginAction()
    {
        $user = $this->getUser();
        if (is_object($user)) {
            if ($user instanceof UserAdmin) {
                return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
            }
            return $this->redirect($this->generateUrl('homepage'));
        }
        
        $request = $this->getRequest();
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        $referer = $request->server->get('HTTP_REFERER');
        
        return array(
            // last username entered by the user
            'last_username' => $session->get(SecurityContext::LAST_USERNAME),
            'error'         => $error,
            'target_path' => $referer ? $referer : 'login_target',
        );
    }
    
    /**
     * @Route("/login_check", name="login_check")
     * @Template()
     */
    public function loginCheckAction()
    {
        return array();
    }
    
    /**
     * @Route("/logout", name="logout")
     * @Template()
     */
    public function logoutAction()
    {
        return array();
    }
    
    /**
     * @Route("/login/target", name="login_target")
     */
    public function loginTargetAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        if (is_object($user) && $user instanceof UserAdmin) {
            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        }
        // TODO: redirect to referer?
        return $this->redirect($this->generateUrl('homepage'));
    }
    
}
