<?php

namespace RMS\RecoveryBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Jun Rama <j.rama@arcanys.com>
 * @Route("/report")
 */
class ReportController extends Controller
{

    /**
     * @Route("/", name="report")
     * @Template()
     */
    public function indexAction()
    {        
        return array();
    }
    
    /**
     * @Route("/global/{date}", name="report_global")
     * 
     */
    public function globalAction($date = false) 
    {
        $em = $this->getDoctrine()->getManager();
        
        $aidsQuery = $em->getRepository('RMSRecoveryBundle:Aid')
            ->createQueryBuilder('a')
            ->where('1=1'); 
            
        $aidsNeededQuery = $em->getRepository('RMSRecoveryBundle:AidNeeded')
            ->createQueryBuilder('an')
            ->where('1=1');        
        
        return $this->buildPDF( 
                    $aidsQuery, 
                    $aidsNeededQuery,
                    $date
                );
    }
    
    private function formatDate( $date )
    {
        $date_parts = explode( ' ', $date );
        $parts = explode('-', $date_parts[0]);
        return $parts[2].'-'.$parts[0].'-'.$parts[1].' '.$date_parts[1];
    }
    
    private function buildPDF( $aidsQuery, $aidsNeededQuery, $date )
    {
        require_once($this->get('kernel')->getRootDir().'/config/dompdf_config.inc.php');

        $dompdf = new \DOMPDF();
        
        $start = empty($date) ? date('Y-m-d 00:00:00') : $this->formatDate($date . ' 00:00:00');
        $end = empty($date) ? date('Y-m-d 23:59:59') : $this->formatDate($date . ' 23:59:59');
        
        $aids = $aidsQuery->andWhere(' a.dateCreated BETWEEN :start AND :end ')
                    ->setParameter('start', $start)
                    ->setParameter('end', $end)
                    ->orderBy('a.dateCreated', 'DESC')
                    ->getQuery()
                    ->getResult();

        $aidsNeeded = $aidsNeededQuery->andWhere(' an.dateCreated BETWEEN :start AND :end ')
                    ->setParameter('start', $start)
                    ->setParameter('end', $end)
                    ->orderBy('an.dateCreated', 'DESC')
                    ->getQuery()
                    ->getResult();
        
        $aids = $this->orderDetails($aids);
        $aidsNeeded = $this->orderDetails($aidsNeeded);
        
        $templating = $this->get('templating');
        
        $logo = $this->get('kernel')->getRootDir().'/../web/bundles/rmsrecovery/images/logo_small.png';
        
        $logo_placeholder = $this->get('kernel')->getRootDir().'/../web/bundles/rmsrecovery/images/logo-placeholder.jpg';
        
        $logo = 'data:image/png;base64,' . base64_encode(file_get_contents($logo));
        $logo_placeholder = 'data:image/jpeg;base64,' . base64_encode(file_get_contents($logo_placeholder));
        
        $html = $templating->render(
            'RMSRecoveryBundle:Report:report.html.twig',
            array(
                'aids' => $aids,
                'aidsNeeded' => $aidsNeeded,
                'logo' => $logo,
                'placeholder' => $logo_placeholder
            )
        );      
        
        //echo $html; exit;
        $dompdf->load_html($html);
        $dompdf->render();

        return new Response($dompdf->output(), 200, array(
            'Content-Type' => 'application/pdf'
        ));
        
    }
    
    private function orderDetails($data)
    {
        $ordered = array();
        foreach ( $data as $datum )
        {
            $created = $datum->getDateCreated()->format("F j, Y");           
            
            $geo = $datum->getGeo();
            if ( $geo instanceof \RMS\RecoveryBundle\Entity\Barangay )
            {
                $barangay = $geo;
                $municipality = $barangay->getMunicipality();            
                $province = $municipality->getProvince();                
                $ordered[$created][$province->getName()]['municipalities'][$municipality->getName()]['barangays'][$barangay->getName()][] = $datum;
            } elseif ( $geo instanceof \RMS\RecoveryBundle\Entity\Municipality ) {
                $municipality = $geo;
                $province = $municipality->getProvince();
                $ordered[$created][$province->getName()]['municipalities'][$municipality->getName()]['aid'][] = $datum;
            } elseif ( $geo instanceof \RMS\RecoveryBundle\Entity\Province ) {
                $province = $geo;
                $ordered[$created][$province->getName()]['aid'][] = $datum;
            }
        }
        
        return $ordered;
    }
    
    /**
     * @Route("/organization/{date}", name="report_organization")
     * @Template()
     */
    public function organizationAction($date = false) 
    {
        $ngo_id = $this->get('security.context')->getToken()->getUser()->getNgo()->getId();
        $user_id = $this->get('security.context')->getToken()->getUser()->getId();

        $em = $this->getDoctrine()->getManager();
        
        $aidsQuery = $em->getRepository('RMSRecoveryBundle:Aid')
            ->createQueryBuilder('a')
            ->where(' a.ngo = :ngo ')
            ->setParameter('ngo', $ngo_id); 
            
        $aidsNeededQuery = $em->getRepository('RMSRecoveryBundle:AidNeeded')
            ->createQueryBuilder('an')
            ->where(' an.reporter = :reporter ')
            ->setParameter('reporter', $user_id);             
        
        return $this->buildPDF( 
                    $aidsQuery, 
                    $aidsNeededQuery,
                    $date
                );      
    }
}
