<?php

namespace RMS\RecoveryBundle\Controller;

use Doctrine\DBAL\DBALException;
use RMS\RecoveryBundle\Form\NgoRegistration\NgoRegistrationType;
use RMS\RecoveryBundle\Model\NgoRegistration;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class NgoRegistrationController extends Controller
{

    /**
     * @Route("/register", name="register")
     * @Template()
     */
    public function registerAction(Request $request)
    {
        $model = new NgoRegistration();
        $form = $this->createForm(new NgoRegistrationType(), $model);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $ngo = $model->getNgo();
            $user = $model->getUser();
            $user->setNgo($ngo);

            try {
                $em->persist($ngo);
                $em->persist($user);
                $em->flush();

                return $this->redirect($this->generateUrl('register_confirmation'));
            } catch(DBALException $ex) {
                $this->get('logger')->err($ex->getMessage());
                $error = new FormError('This value is already used.');
                $form->get('user')->get('username')->addError($error);
            }
        }

        return array(
            'entity' => $model,
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route("/register/confirmation", name="register_confirmation")
     * @Template()
     */
    public function confirmationAction()
    {
        return array(
            
        );
    }

}
