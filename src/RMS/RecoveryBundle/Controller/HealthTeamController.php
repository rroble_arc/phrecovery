<?php

namespace RMS\RecoveryBundle\Controller;

use RMS\RecoveryBundle\Entity\HealthTeam;
use RMS\RecoveryBundle\Entity\UserNgo;
use RMS\RecoveryBundle\Entity\HealthLocation;
use RMS\RecoveryBundle\Form\HealthTeamType;
use RMS\RecoveryBundle\Form\HealthLocationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @author Dottie Decano <d.decano@arcanys.com>
 * @Route("/health/team")
 */
class HealthTeamController extends Controller
{

    /**
     * @Route("/distribution", name="health_team_distribution")
     * @Template()
     */
    public function distributionAction()
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('RMSRecoveryBundle:HealthTeam')->createQueryBuilder('t');
        $qb->select('t')
                ->leftJoin('t.location', 'l')
                ->leftJoin('l.province', 'p')
                ->orderBy('l.name', 'ASC')
                ->addOrderBy('t.name', 'ASC');

        $request = $this->getRequest();
        $session = $request->getSession();
        $filters = $session->get($filterKey = 'attr_hd_province');
        if ($request->query->has('_p')) {
            $filters = array($request->get('_p'));
            $session->set($filterKey, $filters);
        }
        if ($filters) {
            $qb->where($qb->expr()->in('p.id', $filters));
        } else {
            $qb->setMaxResults(0);
        }

        $locations = array();
        $total = array();
        $teams = $qb->getQuery()->getResult();
        foreach ($teams as $team) {
			$name = $team->getLocation()->getName();
            $location = $team->getLocation();
            if (!isset($locations[$name])) {
                $locations[$name] = array(
                    'location' => $location,
                    'teams' => array(),
                );
            }
            $locations[$name]['teams'][] = $team;

			if (!isset($total[$name]))
                $total[$name] = 0;
            $total[$name] += $team->getTotal();
        }

        $provinces = $em->getRepository('RMSRecoveryBundle:Province')
                ->findBy(array(), array('name' => 'ASC'));
        $result = array(
            'locations' => $locations,
            'total' => $total,
            'provinces' => $provinces,
            'filters' => $filters,
        );

        if ($request->isXmlHttpRequest()) {
            return $this->render('RMSRecoveryBundle:HealthTeam:distributionContent.html.twig', $result);
        }

        return $result;
    }

    /**
     * @Route("/manage", name="health_team_manage")
     * @Template()
     */
    public function manageAction()
    {
        $user = $this->getUser();
        if (is_object($user)) {
            if (!$user instanceof UserNgo || !$user->isCoordinator()) {
                throw new AccessDeniedException();
            }
        }
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('RMSRecoveryBundle:HealthTeam')->createQueryBuilder('t');
        $teams = $qb->select('t')
                //->where($qb->expr()->eq('t.ngo', $this->getUser()->getNgo()->getId()))
                ->getQuery()
                ->getResult();

        return array(
            'teams' => $teams,
        );
    }

    /**
     * @Route("/{id}/edit", name="edit_health_team")
     * @Template()
     */
    public function editHealthTeamAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $team = $em->getRepository('RMSRecoveryBundle:HealthTeam')->find($id);

        if (!$team) {
            $this->get('session')->getFlashBag()
                    ->add('notice', "Health Team $id does not exist.");
            return $this->redirect($this->generateUrl('health_team_manage'));
        }

        $form = $this->createForm(new HealthTeamType(), $team, array('request' => $request));
        if ($request->isMethod('post'))
		{
            $form->handleRequest($request);
            if ($form->isValid())
			{
                $em = $this->getDoctrine()->getManager();
                $em->persist($team);
                $em->flush();
 				$this->get('session')->getFlashBag()->add(
						'success', " Team Successfully Updated."
				);
                return $this->redirect($this->generateUrl('health_team_manage'));
            }
        }

		$location = new HealthLocation();
		$form2 = $this->createForm(new HealthLocationType(), $location, array('request' => $request));

        return array(
            'team' => $team,
            'form' => $form->createView(),
			'form2' => $form2->createView(),
        );
    }

    /**
     * @Route("/add", name="add_health_team")
     * @Template()
     */
    public function addHealthTeamAction(Request $request)
    {
        $user = $this->getUser();
        $team = new HealthTeam();
        $team->setNgo($user->getNgo());

        $form = $this->createForm(new HealthTeamType(), $team, array('request' => $request));

        if ($request->isMethod('post'))
        {
            $form->handleRequest($request);
            if ($form->isValid())
            {
                $em = $this->getDoctrine()->getManager();
                $em->persist($team);
                $em->flush();
                return $this->redirect($this->generateUrl('add_health_team_success'));
            }
        }

		$location = new HealthLocation();
		$form2 = $this->createForm(new HealthLocationType(), $location, array('request' => $request));

        return array(
            'form' => $form->createView(),
            'form2' => $form2->createView(),
            'team' => $team,
        );
    }

    /**
     * @Route("/{id}/delete", name="delete_health_team")
     * @Template()
     */
    public function deleteHealthTeamAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $team = $em->getRepository('RMSRecoveryBundle:HealthTeam')->find($id);

        if (!$team) {
            $this->get('session')->getFlashBag()
                    ->add('notice', "Health Team $id does not exist.");
            return $this->redirect($this->generateUrl('health_team_manage'));
        }

        if ($request->isMethod('post'))
		{
			$em = $this->getDoctrine()->getManager();
			$em->remove($team);
			$em->flush();
			$this->get('session')->getFlashBag()->add(
					'success', "! Team Deleted Successfully."
			);
			if( $request->isXmlHttpRequest() )
				return new Response();
			return $this->redirect($this->generateUrl('health_team_manage'));
        }

        return array(
            'team' => $team,
        );
    }

    /**
     * @Route("/add-success", name="add_health_team_success")
     * @Template()
     */
    public function successAction()
    {
        return array();
    }

    /**
     * @Route("/add-location-process", name="add_health_location_process")
     * @Template()
     */
    public function addHealthLocationProcessAction(Request $request)
    {
        $location = new HealthLocation();

        $form = $this->createForm(new HealthLocationType(), $location, array('request' => $request));
        if ($request->isMethod('post'))
        {
            $form->submit($request);
			if ($form->isValid())
			{
				$em = $this->getDoctrine()->getManager();
				$em->persist($location);
				$em->flush();

				$team = new HealthTeam();

				$location = $em->getRepository('RMSRecoveryBundle:HealthLocation')->find($location->getId());
				$team->setLocation($location);

				$form = $this->createForm(new HealthTeamType(), $team, array('request' => $request));

				$success = true;
				$html = $this->renderView("RMSRecoveryBundle:HealthTeam:addHealthLocationProcess.html.twig", array(
					'form' => $form->createView(),
				));
			}
			else
			{
				$success = false;
				$html = "This value is already used.";
			}

			return new \Symfony\Component\HttpFoundation\JsonResponse(array(
				'success' => $success,
				'data' => $html,
			));
        }

        return array(
            'form' => $form->createView(),
        );
    }
}