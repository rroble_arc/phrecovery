<?php

namespace RMS\RecoveryBundle\Controller;

use Doctrine\ORM\Tools\Pagination\Paginator;
use RMS\RecoveryBundle\Entity\HelpCenter;
use RMS\RecoveryBundle\Entity\HelpCenterResponse;
use RMS\RecoveryBundle\Entity\UserNgo;
use RMS\RecoveryBundle\Form\HelpCenterType;
use RMS\RecoveryBundle\Form\HelpCenterResponseType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Jun Rama <j.rama@arcanys.com>
 * @Route("/helpcenter")
 */
class HelpcenterController extends Controller
{

    /**
     * @Route("/", name="helpcenter")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $viewState = $this->getData('helpcenter', 'status');
        if (empty($viewState)) 
        {
            $viewState = $this->setData('helpcenter', 'status', HelpCenter::STATUS_ACTIVE);
        }
        
        $helpcenter = $this->fetchHelpEntries();
        
        $provinces = $em->getRepository('RMSRecoveryBundle:Province')
                ->findBy(array(), array('name' => 'ASC'));
                
        $resources = $em->getRepository('RMSRecoveryBundle:ResourceType')
                        ->findBy(array(), array('name' => 'ASC'));

        $user = $this->getUser();
        if ($user instanceof UserNgo) {
            $help = new HelpCenter();
            $help->setReporter($user);
            
            $response = new HelpCenterResponse();
            $response->setReporter($user);
            $response_form = $this->createForm(new HelpCenterResponseType(), $response);

            $form = $this->createForm(new HelpCenterType(), $help);

            if ($request->isMethod('post')) {
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $em->persist($help);
                    $em->flush();
                    
                    return $this->redirect($this->generateUrl('helpcenter'));
                }
            }

            return array(
                'provinces' => $provinces,
                'resources' => $resources,
                'helpcenter' => $helpcenter['data'],
                'paginator' => $helpcenter['pages'],
                'current_page' => 1,
                'list_view' => $viewState == HelpCenter::STATUS_ACTIVE,
                'form' => $form->createView(),
                'responseform' => $response_form->createView()
            );
        }

        return array(
            'provinces' => $provinces,
            'resources' => $resources,
            'helpcenter' => $helpcenter['data'],
            'paginator' => $helpcenter['pages'],
            'current_page' => 1
        );
    }
    
    protected function fetchHelpEntries(array $provinces=null,array $resources=null, $page=null)
    {
        $ACTIVE_RECORDS = 2; // in days
        $PAGE_LIMIT = 5;
        
        $start = date('Y-m-d H:i:s', (time() - ($ACTIVE_RECORDS * 86600)));
        
        $helpcenter = $this->getDoctrine()->getManager()
                ->getRepository('RMSRecoveryBundle:HelpCenter')
                ->createQueryBuilder('h')
                ->where('h.status = :status')
                ->andWhere('h.dateCreated >= :start')
                ->setParameter('status', $this->getData('helpcenter', 'status'))
                ->setParameter('start', $start);
        if (isset($provinces[0]))
        {
            $ids = $this->fetchProvinceRecords( $provinces );
            if (isset($ids[0]))
            {
                $helpcenter->andWhere('h.id IN (:ids)')
                        ->setParameter('ids', $ids);
            } else {
                $helpcenter->andWhere('0=1'); // force false
            }
        }
        if (isset($resources[0]))
        {
            $helpcenter->join('h.type', 'r')
                        ->andWhere('r.id IN (:resources)')
                        ->setParameter('resources', $resources);
        }
        if (!empty($page))
        {
            $helpcenter->setFirstResult( ($PAGE_LIMIT*($page-1)) );
        }
        $helpcenter->setMaxResults($PAGE_LIMIT)
                    ->orderBy('h.dateCreated', 'DESC');
        
        $paginator = new Paginator($helpcenter, $fetchJoinCollection = true);

        return array( 'pages' => ceil($paginator->count()/$PAGE_LIMIT),
                      'data' => $helpcenter->getQuery()->getResult() );
    }
    
    protected function fetchProvinceRecords(array $provinces=null)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        
        $ACTIVE_RECORDS = 3; // in days
        $PAGE_LIMIT = 5;
        
        $start = date('Y-m-d H:i:s', (time() - ($ACTIVE_RECORDS * 86600)));
                
        $join = array();
        $where = array();
        $offset = '';
        if (isset($provinces[0]))
        {
            $val = implode(',', $provinces);
            $join[] = ' LEFT JOIN geo_province p ON h.geo_id = p.id ';
            $join[] = ' LEFT JOIN geo_municipality m ON h.geo_id = m.id ';
            $where[] = ' AND (p.id IN ('.$val.') OR m.province_id IN ('.$val.')) ';            
        }
                
        $sql = '
            SELECT 
                h.id
            FROM 
                help_center h         
            '.implode(' ', $join).'
            WHERE
                h.status = "'.$this->getData('helpcenter', 'status').'"
                AND h.date_created >= "'.$start.'"
                '.implode(' ', $where)
            ;
        //echo $sql; exit;
        $statement = $connection->prepare($sql);
        $statement->execute();
        $records = $statement->fetchAll();
        
        $ids = array();
        foreach ($records as $val)
        {
            $ids[] = $val['id'];
        }
        
        return $ids;
    }
    
    /**
     * @Route("/filter", name="filter_posts")
     * @Template()
     */
    public function filterAction( Request $request )
    {
        $em = $this->getDoctrine()->getManager();
        
        $provinces = $request->request->get('provinces');
        $provinces = array_filter($provinces, function($a){
            return intval($a);
        });
        $resources = $request->request->get('resources');
        $resources = array_filter($resources, function($a){
            return intval($a);
        });
        $page = $request->request->get('page');  
     
        $helpcenter = $this->fetchHelpEntries($provinces, $resources, $page);
        
        return array(
            'helpcenter' => $helpcenter['data'],
            'paginator' => $helpcenter['pages'],
            'current_page' => (!empty($page)?$page:1)
        );
    }
    
    /**
     * @Route("/response", name="post_response")
     * 
     */
    public function responseAction(Request $request)
    {
        $status = 'error';
        if ($request->isMethod('post')) {
            $em = $this->getDoctrine()->getManager();
            
            $id = $request->request->get('post_id');
            $post = $em->getRepository('RMSRecoveryBundle:HelpCenter')->find($id);
            $user = $this->getUser();
            
            $response = new HelpCenterResponse();
            $response->setReporter( $user );
            $response->setPost( $post );

            $form = $this->createForm(new HelpCenterResponseType(), $response);
            
            $form->handleRequest($request);
           
            if ($form->isValid()) {
                $post->addResponse( $response );
                $em->persist( $post );
                $em->persist( $response );
                $em->flush();
                
                $status = 'success';
                $data = $this->render(
                    'RMSRecoveryBundle:HelpCenter:response.html.twig',
                    array('response' => $response)
                )->getContent();               
            } else {
                $data = $form->getErrors();
            }
        } else {
            $data = 'Invalid parameters!';
        }
        
        $response = new Response();
        $response->setContent(
            json_encode(
                array(
                'status' => $status,
                'data' => $data
                )
            )
        );
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
    }
    
    /**
     * @Route("/archive", name="post_archive")
     * 
     */
    public function archiveAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
            
        $id = $request->request->get('post_id');
        $post = $em->getRepository('RMSRecoveryBundle:HelpCenter')->find($id);
        $user = $this->getUser();
        
        if ( $user->getId() != $post->getReporter()->getId() )
            $this->createNotFoundException("Post <$id> does not belong to you.");
            
        $post->setStatus(HelpCenter::STATUS_NEEDS_MET);
        $em->persist($post);
        $em->flush();
    
        return $this->redirect($this->generateUrl('helpcenter'));
    }
    
    /**
     * @Route("/delete", name="post_delete")
     * 
     */
    public function deleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
            
        $id = $request->request->get('post_id');
        $post = $em->getRepository('RMSRecoveryBundle:HelpCenter')->find($id);
        $user = $this->getUser();
        
        if ( count($post->getResponses()) > 0 )
            $this->createNotFoundException("Post <$id> can not be deleted due to responses.");
               
        $em->remove($post);
        $em->flush();     
        
        return $this->redirect($this->generateUrl('helpcenter'));
    }
    
    /**
     * @Route("/view/archive", name="view_archive")
     * 
     */
    public function viewArchiveAction(Request $request)
    {
        $this->setData('helpcenter', 'status', HelpCenter::STATUS_NEEDS_MET);
        
        return $this->redirect($this->generateUrl('helpcenter'));
    }
    
     /**
     * @Route("/view/current", name="view_current")
     * 
     */
    public function viewCurrentAction(Request $request)
    {
        $this->setData('helpcenter', 'status', HelpCenter::STATUS_ACTIVE);        
        
        return $this->redirect($this->generateUrl('helpcenter'));
    }
    
    private function getAttribute($name, $default = array())
    {
        $session = $this->getRequest()->getSession();
        if ($session->has($key = 'attr_'.$name)) {
            return $session->get($key);
        }
        return $this->setAttribute($name, $default);
    }
    
    private function setAttribute($name, $value)
    {
        $session = $this->getRequest()->getSession();
        $session->set('attr_'.$name, $value);
        return $value;
    }
    
    private function getData($attr, $key = null, $default = null)
    {
        $attrib = $this->getAttribute($attr);
        if ($key) {
            if (isset($attrib[$key])) {
                return $attrib[$key];
            }
            return $default;
        }
        return $attrib;
    }
    
    private function setData($attr, $key, $value)
    {
        $attrib = $this->getAttribute($attr);
        $attrib[$key] = $value;
        $this->setAttribute($attr, $attrib);
    }
}
