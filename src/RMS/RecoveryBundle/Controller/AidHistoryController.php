<?php

namespace RMS\RecoveryBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @author Nico Pag-ong <n.pagong@arcanys.com>
 * @Route("/aid")
 * @method \RMS\RecoveryBundle\Entity\UserNgo getUser()
 */
class AidHistoryController extends Controller
{

    /**
     * @Route("/history", name="aid_history")
     * @Template()
     */
    public function historyAction()
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $aids = $em->getRepository('RMSRecoveryBundle:Aid')
                ->findBy(array('ngo' => $user->getNgo()->getId()), 
                        array('dateStart' => 'DESC'));

        return array(
            'aids' => $aids,
        );
    }

}
