<?php

namespace RMS\RecoveryBundle\Controller;

use RMS\RecoveryBundle\Entity\Aid;
use RMS\RecoveryBundle\Form\AidEntryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 * @Route("/aid")
 * @method \RMS\RecoveryBundle\Entity\UserNgo getUser()
 */
class AidEntryController extends Controller
{

    /**
     * @Route("/entry", name="aid_entry")
     * @Template()
     */
    public function addEntryAction(Request $request)
    {
        $user = $this->getUser();
        $aid = new Aid();
        $aid->setNgo($user->getNgo());

        $form = $this->createForm(new AidEntryType(), $aid, array('request' => $request));

        if ($request->isMethod('post'))
        {
            $form->handleRequest($request);
            if ($form->isValid())
            {
                $em = $this->getDoctrine()->getManager();
                $em->persist($aid);
                $em->flush();
                return $this->redirect($this->generateUrl('aid_entry_success'));
            }
        }

        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/entry/{id}/edit", name="edit_aid_entry")
     * @Template()
     */
    public function editEntryAction(Request $request, $id)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $aid = $em->getRepository('RMSRecoveryBundle:Aid')->find($id);
        
        if (!$aid)
        {
            $this->get('session')->getFlashBag()->add(
                    'notice', "Aid $id does not exits."
            );
            return $this->redirect($this->generateUrl('aid_history'));
        }
        
        $is_owner = $aid->belongsToNgo($user->getNgo());
        
        if (!$is_owner)
        {
            $this->get('session')->getFlashBag()->add(
                    'notice', "Aid Entry $id does not Belong to your Organization."
            );
            return $this->redirect($this->generateUrl('aid_history'));
        }

        $form = $this->createForm(new AidEntryType(), $aid, array('request' => $request));

        if ($request->isMethod('post'))
        {
            $form->handleRequest($request);
            if ($form->isValid())
            {
                $em = $this->getDoctrine()->getManager();
                $em->persist($aid);
                $em->flush();
                $this->get('session')->getFlashBag()->add(
                        'success', "Aid Successfully Saved!."
                );
                return $this->redirect($this->generateUrl('aid_history'));
            }
        }

        return array(
            'aid' => $aid,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/entry-success", name="aid_entry_success")
     * @Template()
     */
    public function successAction()
    {
        return array();
    }

    /**
     * @Route("/options/municipalities", name="geo_options_municipality")
     * @Template()
     */
    public function municipalityOptionsAction()
    {
        $id = $this->getRequest()->get('id');
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('RMSRecoveryBundle:Municipality')
                ->findBy(array('province' => $id), array('name' => 'ASC'));
        $result = array();
        foreach ((array) $entities as $entity)
        {
            $result[] = array(
                'id' => $entity->getId(),
                'name' => $entity->getName()
            );
        }
        return new JsonResponse($result);
    }

    /**
     * @Route("/options/barangays", name="geo_options_barangay")
     * @Template()
     */
    public function barangayOptionsAction()
    {
        $id = $this->getRequest()->get('id');
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('RMSRecoveryBundle:Barangay')
                ->findBy(array('municipality' => $id), array('name' => 'ASC'));
        $result = array();
        foreach ((array) $entities as $entity)
        {
            $result[] = array(
                'id' => $entity->getId(),
                'name' => $entity->getName()
            );
        }
        return new JsonResponse($result);
    }

}
