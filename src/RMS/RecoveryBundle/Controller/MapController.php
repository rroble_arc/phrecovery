<?php

namespace RMS\RecoveryBundle\Controller;

use RMS\RecoveryBundle\Entity\Geo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Jun Rama <j.rama@arcanys.com>
 * @Route("/map")
 */
class MapController extends Controller
{

    /**
     * @Route("/", name="map")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $municipalities = $em->getRepository('RMSRecoveryBundle:Municipality')
            ->findBy(array(), array('name' => 'ASC'));

        $qb = $em->getRepository('RMSRecoveryBundle:Municipality')->createQueryBuilder('m');
        $qb->select('m')
                ->leftJoin('m.province', 'p');

        $request = $this->getRequest();
        $session = $request->getSession();
        $filters = $session->get($filterKey = 'attr_hd_province');
        if ($request->query->has('_p')) {
            $filters = array($request->get('_p'));
            $session->set($filterKey, $filters);
        }
        if ($filters) {
            $qb->where($qb->expr()->in('p.id', $filters));
        } else {
            $qb->setMaxResults(0);
        }

		$municipalities = $qb->getQuery()->getResult();

		$provinces = $em->getRepository('RMSRecoveryBundle:Province')
                ->findBy(array(), array('name' => 'ASC'));

		$result = array(
            'municipalities' => $municipalities,
			'provinces' => $provinces,
			'filters' => $filters,
        );

        if ($request->isXmlHttpRequest()) {
			return new \Symfony\Component\HttpFoundation\JsonResponse(array(
				'mapMarkers' => $this->getMapMarkers( $municipalities ),
			));
        }
		return $result;
    }

	public function getMapMarkers($municipalities)
	{
		$barangayMarkers = array();
		$municipalityMarkers = array();
		foreach( $municipalities as $municipality )
		{
			$municipality_coords = $municipality->getCoords();
			if( $municipality_coords )
			{
				$coords_arr = array();
				foreach( $municipality_coords as $coords )
				{
					$coords_arr[] = array(
						'lat' => $coords->lat,
						'lng' => $coords->lng
					);
				}
				$municipalityMarkers[] = array(
					'coords' => $coords_arr,
					'info' => array(
						'municipality-name' => $municipality->getName(),
						'municipality-population' => $municipality->getPopulation(),
						'municipality-estimated-households' => $municipality->getEstimatedHouseholds(),
						'municipality-affected-households' => $municipality->getAffectedHouseholds(),
					)
				);
			}

			$barangays = $municipality->getBarangays();
			if( $barangays )
			{
				foreach( $barangays as $barangay )
				{
					$barangay_coords = $barangay->getCoords();
					if( $barangay_coords )
					{
						foreach( $barangay_coords as $coords )
						{
							//var_dump($barangay_coords);
//							array (size=1)
//							0 =>
//							object(stdClass)[1993]
//							public 'lat' => string '10.46852628230' (length=14)
//							public 'lng' => string '121.92304811400' (length=15)
							$barangayMarkers[] = array(
								'lat' => $coords->lat,
								'lng' => $coords->lng,
								'info' => array(
									'barangay-name' => $barangay->getName(),
									'barangay-population' => $barangay->getPopulation(),
									'barangay-estimated-households' => $barangay->getEstimatedHouseholds(),
									'barangay-affected-households' => $barangay->getAffectedHouseholds(),
								)
							);
						}
					}
				}
			}
		}
		return array(
			'barangayMarkers' => $barangayMarkers,
			'municipalityMarkers' => $municipalityMarkers
		);
	}

    /**
     * @Route("/set-map", name="set_map")
     * @Template()
     */
    public function setMapAction()
    {
		echo 'test';

		$result = array(
            'test' => 'test'
        );
		return $result;
    }
}
