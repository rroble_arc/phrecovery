<?php

namespace RMS\RecoveryBundle\Controller;

use RMS\RecoveryBundle\Entity\HealthStatus;
use RMS\RecoveryBundle\Form\HealthStatusType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Dottie Decano <d.decano@arcanys.com>
 * @Route("/health/status")
 */
class HealthStatusController extends Controller
{

    /**
     * @Route("/", name="health_status")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('RMSRecoveryBundle:HealthStatus')->createQueryBuilder('s');
        $qb->select('s')
                ->leftJoin('s.location', 'l')
                ->leftJoin('l.province', 'p')
                ->orderBy('l.name', 'ASC')
                ->addOrderBy('s.dateCreated', 'DESC');


        $request = $this->getRequest();
        $session = $request->getSession();
        $filters = $session->get($filterKey = 'attr_hd_province');
        if ($request->query->has('_p')) {
            $filters = array($request->get('_p'));
            $session->set($filterKey, $filters);
        }
        if ($filters) {
            $qb->where($qb->expr()->in('p.id', $filters));
        } else {
            $qb->setMaxResults(0);
        }

        $locations = array();
		$list = $qb->getQuery()->getResult();
        foreach ($list as $status) {
            $location = $status->getLocation();
            $id = $location->getId();
            if (!isset($locations[$id])) {
                $locations[$id] = array(
                    'location' => $location,
                    'messages' => array(),
                );
            }
            $locations[$id]['messages'][] = $status;
        }
        $provinces = $em->getRepository('RMSRecoveryBundle:Province')
                ->findBy(array(), array('name' => 'ASC'));
        $result = array(
            'locations' => $locations,
			'provinces' => $provinces,
			'filters' => $filters,
        );
        if ($request->isXmlHttpRequest()) {
            return $this->render('RMSRecoveryBundle:HealthStatus:statusContent.html.twig', $result);
        }
		return $result;
    }

    /**
     * @Route("/add", name="add_health_status")
     * @Template()
     */
    public function addHealthStatusAction(Request $request)
    {
        $user = $this->getUser();
        $status = new HealthStatus();
        $status->setReporter($user->getNgo());

        $form = $this->createForm(new HealthStatusType(), $status, array('request' => $request));

        if ($request->isMethod('post'))
        {
            $form->handleRequest($request);
            if ($form->isValid())
            {
                $em = $this->getDoctrine()->getManager();
                $em->persist($status);
                $em->flush();
                return $this->redirect($this->generateUrl('add_health_status_success'));
            }
        }

        return array(
            'form' => $form->createView(),
            'status' => $status,
        );
    }

    /**
     * @Route("/add-success", name="add_health_status_success")
     * @Template()
     */
    public function successAction()
    {
        return array();
    }
}
