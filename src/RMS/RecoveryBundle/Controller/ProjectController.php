<?php

namespace RMS\RecoveryBundle\Controller;

use RMS\RecoveryBundle\Entity\Project;
use RMS\RecoveryBundle\Form\ProjectType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @author Dottie Decano <d.decano@arcanys.com>
 * @Route("/volunteering")
 * @method \RMS\RecoveryBundle\Entity\UserNgo getUser()
 */
class ProjectController extends Controller
{
    /**
     * @Route("/list", name="volunteering")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->getRepository('RMSRecoveryBundle:Project')->createQueryBuilder('pr');
        $qb->leftJoin('pr.geo', 'g')
			->orderBy('pr.dateStart', 'ASC');

        $session = $request->getSession();
        $filters = $session->get($filterKey = 'attr_hd_province');
		$test = $request->get('_p');
		if(
			( $request->query->has('_p') && $request->get('_p') == 0 ) ||
			$request->query->has('_p') == ''
		)
		{
			$filters = array(0);
			//echo ("empty _p:".$test);
		}
		else
		{
			//echo ("_p:".$test);
			$filters = array($request->get('_p'));
			/* @var $province \RMS\RecoveryBundle\Entity\Province */
			$province = $em->getRepository('RMSRecoveryBundle:Province')->find($filters[0]);
			$filters = $province->getTreeIds();
			$qb->where($qb->expr()->in('g.id', $filters));
		}

		$session->set($filterKey, $filters);

        $projects = $qb->getQuery()->getResult();
        $provinces = $em->getRepository('RMSRecoveryBundle:Province')
                ->findBy(array(), array('name' => 'ASC'));

		$result = array(
            'projects' => $projects,
            'provinces' => $provinces,
            'filters' => $filters,
        );

		//var_dump($projects);
        if ($request->isXmlHttpRequest()) {
            return $this->render('RMSRecoveryBundle:Project:indexContent.html.twig', $result);
        }

        return $result;
    }

    /**
     * @Route("/manage", name="project_manage")
     * @Template()
     */
    public function manageAction()
    {
		$user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

		$projects = $em->getRepository('RMSRecoveryBundle:Project')
					->findBy(array('ngo' => $user->getNgo()->getId() ),
						array('dateStart' => 'ASC'));
        return array(
            'projects' => $projects,
        );
    }

    /**
     * @Route("/{id}/edit", name="edit_project")
     * @Template()
     */
    public function editProjectAction(Request $request, $id)
    {
		$user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $project = $em->getRepository('RMSRecoveryBundle:Project')->find($id);

        if (!$project) {
            $this->get('session')->getFlashBag()
                    ->add('notice', "Project $id does not exist.");
            return $this->redirect($this->generateUrl('volunteering'));
        }

		$is_owner = $project->belongsToNgo($user->getNgo());
		if(!$is_owner)
		{
			$this->get('session')->getFlashBag()->add(
				'notice', "Project $id does not belong to your Organization."
				);
			return $this->redirect($this->generateUrl('volunteering'));
		}

        $form = $this->createForm(new ProjectType(), $project, array('request' => $request));
        if ($request->isMethod('post'))
		{
			if( $request->get('map_markers') )
				$markers = $request->get('map_markers');
			else
				$markers = null;
			$project->setMap($markers);

            $form->handleRequest($request);
            if ($form->isValid())
			{
                $em = $this->getDoctrine()->getManager();
                $em->persist($project);
                $em->flush();
 				$this->get('session')->getFlashBag()->add(
						'success', " Project Successfully Updated."
				);
                return $this->redirect($this->generateUrl('project_manage'));
            }
        }

        return array(
            'project' => $project,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/add", name="project_add")
     * @Template()
     */
    public function addProjectAction(Request $request)
    {
        $user = $this->getUser();
        $project = new Project();
        $project->setNgo($user->getNgo());
        $project->setDateStart(new \DateTime('today'));
        $project->setDateEnd(new \DateTime('today'));

        $form = $this->createForm(new ProjectType(), $project, array('request' => $request));

        if ($request->isMethod('post'))
        {
			if( $request->get('map_markers') )
				$markers = $request->get('map_markers');
			else
				$markers = null;
			$project->setMap($markers);

            $form->handleRequest($request);
            if ($form->isValid())
            {
                $em = $this->getDoctrine()->getManager();
                $em->persist($project);
                $em->flush();
                return $this->redirect($this->generateUrl('add_project_success'));
            }
			else
			{
				var_dump($form->getErrorsAsString());
			}
        }

        return array(
            'form' => $form->createView(),
            'project' => $project,
        );
    }

    /**
     * @Route("/add-success", name="add_project_success")
     * @Template()
     */
    public function successAction()
    {
        return array();
    }
}
