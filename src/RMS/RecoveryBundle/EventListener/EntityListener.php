<?php

namespace RMS\RecoveryBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use RMS\RecoveryBundle\Model\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class EntityListener
{

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof UserInterface) {
            $password = $this->encodePassword($entity, $entity->getPassword(), $entity->getSalt());
            $entity->setPassword($password, false);
        }
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof UserInterface) {
            if ($args->hasChangedField('password')) {
                if ($entity->getPassword()) {
                    $password = $this->encodePassword($entity, $entity->getPassword(), $entity->getSalt());
                    $args->setNewValue('password', $password);
                } else {
                    $args->setNewValue('password', $args->getOldValue('password'));
                    $args->setNewValue('salt', $args->getOldValue('salt'));
                }
            }
        }
    }

    private function encodePassword($entity, $password, $salt)
    {
        $factory = $this->container->get('security.encoder_factory');
        $encoder = $factory->getEncoder($entity);
        return $encoder->encodePassword($password, $salt);
    }

}
