<?php

namespace RMS\RecoveryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Situation
 *
 * @ORM\Table(name="situation")
 * @ORM\Entity
 */
class Situation extends AbstractTimestamptable implements Timestamptable
{

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="reportedSituations", cascade={"persist"})
     * @ORM\JoinColumn(name="reporter", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * 
     * @var User
     */
    protected $reporter;

    /**
     * @ORM\ManyToOne(targetEntity="Geo", inversedBy="situations", cascade={"persist"})
     * @ORM\JoinColumn(name="geo_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * 
     * @var Geo
     */
    protected $geo;

    /**
     * @ORM\ManyToOne(targetEntity="Status", cascade={"persist"})
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * 
     * @var Status
     */
    protected $status;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reporter
     *
     * @param User $reporter
     * @return Situation
     */
    public function setReporter(User $reporter)
    {
        $this->reporter = $reporter;

        return $this;
    }

    /**
     * Get reporter
     *
     * @return User 
     */
    public function getReporter()
    {
        return $this->reporter;
    }

    /**
     * Set geo
     *
     * @param Geo $geo
     * @return Situation
     */
    public function setGeo(Geo $geo)
    {
        $this->geo = $geo;

        return $this;
    }

    /**
     * Get geo
     *
     * @return Geo 
     */
    public function getGeo()
    {
        return $this->geo;
    }

    /**
     * Set status
     *
     * @param Status $status
     * @return Situation
     */
    public function setStatus(Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return Status 
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getDate()
    {
        return $this->dateCreated;
    }
    
    public function __toString()
    {
        return $this->status.'';
    }
    
}
