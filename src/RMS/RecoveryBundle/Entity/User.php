<?php

namespace RMS\RecoveryBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RMS\RecoveryBundle\Model\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * @ORM\Table(name="users", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="unique_username_idx", columns={"username"}),
 *      @ORM\UniqueConstraint(name="unique_email_idx", columns={"email"})
 * })
 * @ORM\Entity()
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *      "user" = "User",
 *      "ngo" = "UserNgo",
 *      "admin" = "UserAdmin"
 * })
 * @UniqueEntity("email")
 * @UniqueEntity("username")
 */
abstract class User extends AbstractTimestamptable implements Timestamptable, UserInterface
{

    const ROLE_DEFAULT = 'ROLE_USER',
            ROLE_NGO = 'ROLE_NGO',
            ROLE_ADMIN = 'ROLE_ADMIN',
            ROLE_SONATA_ADMIN = 'ROLE_SONATA_ADMIN',
            ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(name="username", type="string", length=45, nullable=false)
     * @Assert\NotBlank()
     * 
     * @var string
     */
    protected $username;

    /**
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     * 
     * @var string
     */
    protected $password;

    /**
     * @ORM\Column(name="salt", type="string", length=45, nullable=false)
     * 
     * @var string
     */
    protected $salt;

    /**
     * @ORM\Column(name="firstname", type="string", length=45, nullable=false)
     * @Assert\NotBlank()
     * 
     * @var string
     */
    protected $firstname;

    /**
     * @ORM\Column(name="lastname", type="string", length=45, nullable=false)
     * @Assert\NotBlank()
     * 
     * @var string
     */
    protected $lastname;

    /**
     * @ORM\Column(name="email", type="string", length=45, unique=true, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Email()
     * 
     * @var string
     */
    protected $email;

    /**
     * @ORM\Column(name="roles", type="simple_array", nullable=false)
     * 
     * @var array
     */
    protected $roles;

    /**
     * @ORM\OneToMany(targetEntity="Situation", mappedBy="reporter", cascade={"persist"})
     * 
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $reportedSituations;

    /**
     * @ORM\OneToMany(targetEntity="AidNeeded", mappedBy="reporter", cascade={"persist"})
     * 
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $reportedNeeds;
    
    /**
     * @ORM\Column(name="receive_emails", type="boolean", nullable=true)
     * 
     * @var boolean
     */
    protected $receiveEmails;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reportedSituations = new ArrayCollection();
        $this->reportedNeeds = new ArrayCollection();
        $this->ngos = new ArrayCollection();
        $this->addRole(static::ROLE_DEFAULT);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password, $checkSalt = true)
    {
        if ($checkSalt) {
        // generate new salt
            if ($this->password != $password) {
                $this->getSalt(true);
            }
        }
        
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt($new = false)
    {
        if (!$this->salt || $new) {
            $this->salt = hash('sha1', rand());
        }
        return $this->salt;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set roles
     *
     * @param array $roles
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Get roles
     *
     * @return array 
     */
    public function getRoles()
    {
        $this->addRole(static::ROLE_DEFAULT);
        return array_unique($this->roles);
    }
    
    public function addRole($role)
    {
        $this->roles[] = $role;
        
        return $this;
    }

    /**
     * Get reportedSituations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReportedSituations()
    {
        return $this->reportedSituations;
    }

    /**
     * Get reportedNeeds
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReportedNeeds()
    {
        return $this->reportedNeeds;
    }

    public function __toString()
    {
        return ucwords($this->firstname.' '.$this->lastname);
    }

    /**
     * Add ngo
     *
     * @param Ngo $ngo
     * @return User
     */
    public function addNgo(Ngo $ngo)
    {
        $this->ngos[] = $ngo;

        return $this;
    }

    /**
     * Remove ngo
     *
     * @param Ngo $ngo
     */
    public function removeNgo(Ngo $ngo)
    {
        $this->ngos->removeElement($ngo);
    }
    
    /**
     * @return boolean
     */
    public function getReceiveEmails()
    {
        return $this->receiveEmails;
    }

    public function setReceiveEmails($receiveEmails)
    {
        $this->receiveEmails = $receiveEmails;
    }

    public function eraseCredentials()
    {
        
    }

    public function getName()
    {
        return $this->firstname.' '.$this->lastname;
    }
    
}
