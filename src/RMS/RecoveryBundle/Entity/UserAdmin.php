<?php

namespace RMS\RecoveryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserAdmin
 *
 * @ORM\Table(name="user_admin")
 * @ORM\Entity
 */
class UserAdmin extends User
{

    public function __construct()
    {
        parent::__construct();
        $this->addRole(static::ROLE_ADMIN);
        $this->addRole(static::ROLE_SONATA_ADMIN);
    }

}
