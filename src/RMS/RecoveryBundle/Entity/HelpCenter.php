<?php

namespace RMS\RecoveryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Help Center Post
 * @author Jun Rama <j.rama@arcanys.com>
 *
 * @ORM\Table(name="help_center")
 * @ORM\Entity
 */
class HelpCenter extends AbstractTimestamptable implements Timestamptable
{
    const STATUS_ACTIVE = 'Needs Active',
          STATUS_NEEDS_MET = 'Needs Met',
          STATUS_NEEDS_EXPIRED = 'Needs Expired',
          STATUS_NEEDS_CANCELLED = 'Needs Cancelled';

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserNgo", inversedBy="reportedSituations", cascade={"persist"})
     * @ORM\JoinColumn(name="reporter", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * 
     * @var UserNgo
     */
    protected $reporter;

    /**
     * @ORM\ManyToOne(targetEntity="Geo", inversedBy="aidsNeeded", cascade={"persist"})
     * @ORM\JoinColumn(name="geo_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * 
     * @var Geo
     */
    protected $geo;
 
    /**
     * @ORM\ManyToOne(targetEntity="ResourceType")
     * @ORM\JoinColumn(name="resource_id", referencedColumnName="id")
     *
     * @var ResourceType
     **/
    protected $type;
    
    /**
     * @ORM\OneToMany(targetEntity="HelpCenterResponse", mappedBy="post")
     *
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $responses;
    
    /**
     * @ORM\Column(name="contact", type="text", nullable=true)
     * 
     * @var string
     */
    protected $contact;
    
    /**
     * @ORM\Column(name="message", type="text", nullable=true)
     * 
     * @var string
     */
    protected $message;
     
    /**
     * @ORM\Column(name="status", type="string", length=45, nullable=true)
     * 
     * @var string
     */
    protected $status;
    
    public function __construct()
    {
        $this->setStatus(static::STATUS_ACTIVE);
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reporter
     *
     * @param UserNgo $reporter
     * @return Situation
     */
    public function setReporter(UserNgo $reporter)
    {
        $this->reporter = $reporter;

        return $this;
    }

    /**
     * Get reporter
     *
     * @return HelpCenter 
     */
    public function getReporter()
    {
        return $this->reporter;
    }

    /**
     * Set geo
     *
     * @param Geo $geo
     * @return AidNeeded
     */
    public function setGeo(Geo $geo)
    {
        $this->geo = $geo;

        return $this;
    }

    /**
     * Get geo
     *
     * @return Geo 
     */
    public function getGeo()
    {
        return $this->geo;
    }

    /**
     * Set type
     *
     * @param ResourceType $type
     * @return HelpCenter
     */
    public function setType(ResourceType $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return ResourceType 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add response
     *
     * @param HelpCenterResponse $response
     * @return HelpCenter
     */
    public function addResponse(HelpCenterResponse $response)
    {
        $this->responses[] = $response;
        
        return $this;
    }

    /**
     * Remove response
     *
     * @param HelpCenterResponse $response
     * @return HelpCenter
     */
    public function removeResponse(HelpCenterResponse $response)
    {
        $this->responses->removeElement($response);
    }
    
    /**
     * Get responses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getResponses()
    {
        return $this->responses;
    }
    
    /**
     * Set message
     *
     * @param string $message
     * @return HelpCenter
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }
    
    /**
     * Set contact
     *
     * @param string $contact
     * @return HelpCenter
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string 
     */
    public function getContact()
    {
        return $this->contact;
    }
    
    public function getDate()
    {
        return $this->dateCreated;
    }
    
    public function __toString()
    {
        return $this->message.'';
    }
    
    /**
     * Set status
     *
     * @param string $status
     * @return HelpCenter
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
}
