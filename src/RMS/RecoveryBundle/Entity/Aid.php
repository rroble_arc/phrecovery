<?php

namespace RMS\RecoveryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Aid
 *
 * @ORM\Table(name="aid")
 * @ORM\Entity()
 */
class Aid extends AbstractTimestamptable implements Timestamptable
{

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AidType", cascade={"persist"})
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * 
     * @var AidType
     */
    protected $type;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Assert\Length(max=150)
     * 
     * @var string
     */
    protected $description;

    /**
     * @ORM\Column(name="unit_cost", type="float", nullable=true)
     * 
     * @var float
     */
    protected $unitCost;

    /**
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     * @Assert\GreaterThanOrEqual(1)
     * 
     * @var integer
     */
    protected $quantity;

    /**
     * @ORM\Column(name="people_catered", type="integer", nullable=false)
     * @Assert\GreaterThanOrEqual(1)
     * 
     * @var integer
     */
    protected $peopleCatered;

    /**
     * @ORM\Column(name="date_start", type="date", nullable=false)
     * 
     * @var \DateTime
     */
    protected $dateStart;

    /**
     * @ORM\Column(name="date_end", type="date", nullable=true)
     * 
     * @var \DateTime
     */
    protected $dateEnd;

    /**
     * @ORM\ManyToOne(targetEntity="Geo", cascade={"persist"}, inversedBy="aids")
     * @ORM\JoinColumn(name="geo_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * 
     * @var Geo
     */
    protected $geo;

    /**
     * @ORM\ManyToOne(targetEntity="Ngo", cascade={"persist"}, inversedBy="aids")
     * @ORM\JoinColumn(name="ngo_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * 
     * @var Ngo
     */
    protected $ngo;
     
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param AidType $type
     * @return Aid
     */
    public function setType(AidType $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return AidType 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Aid
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set unitCost
     *
     * @param float $unitCost
     * @return Aid
     */
    public function setUnitCost($unitCost)
    {
        $this->unitCost = $unitCost;

        return $this;
    }

    /**
     * Get unitCost
     *
     * @return float 
     */
    public function getUnitCost()
    {
        return $this->unitCost;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Aid
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set peopleCatered
     *
     * @param integer $peopleCatered
     * @return Aid
     */
    public function setPeopleCatered($peopleCatered)
    {
        $this->peopleCatered = $peopleCatered;

        return $this;
    }

    /**
     * Get peopleCatered
     *
     * @return integer 
     */
    public function getPeopleCatered()
    {
        return $this->peopleCatered;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     * @return Aid
     */
    public function setDateStart(\DateTime $dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime 
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     * @return Aid
     */
    public function setDateEnd(\DateTime $dateEnd = null)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime 
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set geo
     *
     * @param Geo $geo
     * @return Aid
     */
    public function setGeo(Geo $geo)
    {
        $this->geo = $geo;

        return $this;
    }

    /**
     * Get geo
     *
     * @return Geo 
     */
    public function getGeo()
    {
        return $this->geo;
    }

    /**
     * Set ngo
     *
     * @param Ngo $ngo
     * @return Aid
     */
    public function setNgo(Ngo $ngo)
    {
        $this->ngo = $ngo;

        return $this;
    }

    /**
     * Get ngo
     *
     * @return Ngo 
     */
    public function getNgo()
    {
        return $this->ngo;
    }

    public function __toString()
    {
        if (isset($this->description[50])) {
            return substr($this->description, 0, 50). '...';
        }
        return $this->description.'';
    }
    
    /**
     * @return integer
     */
    public function getDuration()
    {
        if ($this->dateStart && $this->dateEnd) {
            $diff = $this->dateStart->diff($this->dateEnd, true);
            return intval($diff->format('%d')) + 1;
        }
    }

    public function setDuration($duration)
    {
        if ($this->dateStart && $duration > 0) {
            $this->dateEnd = clone $this->dateStart;
            $days = $duration - 1;
            $this->dateEnd->add(new \DateInterval("P{$days}D"));
        }
    }
    
    /**
     * @return Province
     */
    public function getProvince()
    {
        if ($this->geo) {
            if ($this->geo->isProvince()) {
                return $this->geo;
            } else if (($mun = $this->getMunicipality())) {
                return $mun->getProvince();
            } else if (($bar = $this->getBarangay())) {
                return $bar->getMunicipality()->getProvince();
            }
        }
    }

    public function setProvince(Province $province)
    {
        return $this;
    }

    /**
     * @return Municipality
     */
    public function getMunicipality()
    {
        if ($this->geo) {
            if ($this->geo->isMunicipality()) {
                return $this->geo;
            } else if (($bar = $this->getBarangay())) {
                return $bar->getMunicipality();
            }
        }
    }

    public function setMunicipality(Municipality $municipality)
    {
        return $this;
    }
    
    /**
     * @return Barangay
     */
    public function getBarangay()
    {
        if ($this->geo && $this->geo->isBarangay()) {
            return $this->geo;
        }
    }
    
    public function belongsToNgo(Ngo $ngo)
    {
        if($ngo->getId() == $this->getNgo()->getId())
        {
            return true;
        }
        return false;
    }
    
}
