<?php

namespace RMS\RecoveryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use RMS\RecoveryBundle\Model\Enums;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Ngo
 *
 * @ORM\Table(name="ngo", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="unique_name_idx", columns={"name"})
 * })
 * @ORM\Entity()
 * @UniqueEntity("name")
 */
class Ngo extends AbstractTimestamptable implements Timestamptable
{

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     * 
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(name="logo", type="string", length=255, nullable=true)
     * @Assert\Url()
     * 
     * @var string
     */
    protected $logo;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Assert\Length(max=200)
     * 
     * @var string
     */
    protected $description;

    /**
     * @ORM\Column(name="street", type="string", length=45, nullable=true)
     * 
     * @var string
     */
    protected $street;

    /**
     * @ORM\Column(name="city", type="string", length=45, nullable=true)
     * 
     * @var string
     */
    protected $city;

    /**
     * @ORM\Column(name="country", type="string", length=45, nullable=true)
     * 
     * @var string
     */
    protected $country;

    /**
     * @ORM\Column(name="zipcode", type="string", length=45, nullable=true)
     * 
     * @var string
     */
    protected $zipcode;

    /**
     * @ORM\Column(name="verification_status", type="boolean", nullable=true)
     * 
     * @var boolean
     */
    protected $status;

    /**
     * @ORM\OneToMany(targetEntity="Aid", mappedBy="ngo", cascade={"persist"})
     * 
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $aids;
    
    /**
     * @ORM\OneToMany(targetEntity="UserNgo", mappedBy="ngo", cascade={"persist"})
     * 
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $users;

    /**
     * @ORM\Column(name="website", type="string", nullable=true, length=255)
     * @Assert\Url()
     * 
     * @var boolean
     */
    protected $website;

    /**
     * @ORM\Column(name="ngo_type", type="string", length=45, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Choice(callback = {"RMS\RecoveryBundle\Model\Enums", "validNgoTypes"})
     * 
     * @var string
     */
    protected $ngoType;

    /**
     * @ORM\Column(name="skills", type="simple_array", nullable=true)
     * 
     * @var array
     */
    protected $skills;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->aids = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->ngoType = Enums::NGO_TYPE_PRIVATE;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Ngo
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return Ngo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string 
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Ngo
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return Ngo
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Ngo
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Ngo
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set zipcode
     *
     * @param string $zipcode
     * @return Ngo
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode
     *
     * @return string 
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Ngo
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * @return boolean
     */
    public function isVerified()
    {
        return $this->status;
    }

    public function __toString()
    {
        return $this->name.'';
    }

    /**
     * Add aid
     *
     * @param Aid $aid
     * @return Ngo
     */
    public function addAid(Aid $aid)
    {
        $this->aids[] = $aid;

        return $this;
    }

    /**
     * Remove aid
     *
     * @param Aid $aid
     */
    public function removeAid(Aid $aid)
    {
        $this->aids->removeElement($aid);
    }

    /**
     * Get aids
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAids()
    {
        return $this->aids;
    }

    /**
     * Add user
     *
     * @param UserNgo $user
     * @return Ngo
     */
    public function addUser(UserNgo $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param UserNgo $user
     */
    public function removeUser(UserNgo $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    public function setWebsite($website)
    {
        $this->website = $website;
    }

    public function getNgoType()
    {
        return $this->ngoType;
    }

    public function setNgoType($ngoType)
    {
        $this->ngoType = $ngoType;
        
        return $this;
    }

    /**
     * @return array
     */
    public function getSkills()
    {
        return $this->skills;
    }

    public function setSkills(array $skills)
    {
        $this->skills = $skills;
        
        return $this;
    }

    /**
     * @return boolean
     */
    public function isHealthNgo()
    {
        return $this->ngoType == Enums::NGO_TYPE_HEALTH;
    }
    
}
