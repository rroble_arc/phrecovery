<?php

namespace RMS\RecoveryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RMS\RecoveryBundle\Model\AidManager;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Geo
 *
 * @ORM\Table(name="geo")
 * @ORM\Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *      "province" = "Province",
 *      "municipality" = "Municipality",
 *      "barangay" = "Barangay",
 * })
 */
abstract class Geo extends AbstractTimestamptable implements Timestamptable
{

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     * @Assert\NotBlank()
     * 
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(name="map", type="text", nullable=true)
     * 
     * @var string
     */
    protected $map;

    /**
     * @ORM\OneToMany(targetEntity="Situation", mappedBy="geo", cascade={"persist"})
     * 
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $situations;

    /**
     * @ORM\OneToMany(targetEntity="MessageBoard", mappedBy="geo", cascade={"persist"})
     * 
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $messages;

    /**
     * @ORM\OneToMany(targetEntity="AidNeeded", mappedBy="geo", cascade={"persist"})
     * 
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $aidsNeeded;

    /**
     * @ORM\OneToMany(targetEntity="Aid", mappedBy="geo", cascade={"persist"})
     * 
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $aids;

    /**
     * @ORM\Column(name="population", type="float", nullable=true)
     * 
     * @var float
     */
    protected $population;

    /**
     * @ORM\Column(name="estimated_households", type="integer", nullable=true)
     * 
     * @var integer
     */
    protected $estimatedHouseholds;

    /**
     * @ORM\Column(name="affected_households", type="integer", nullable=true)
     * 
     * @var integer
     */
    protected $affectedHouseholds;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->situations = new ArrayCollection();
        $this->aidsNeeded = new ArrayCollection();
        $this->aids = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Barangay
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set map
     *
     * @param string $map
     * @return Geo
     */
    public function setMap($map)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * Get map
     *
     * @return string 
     */
    public function getMap()
    {
        return $this->map;
    }

    /**
     * Add situations
     *
     * @param Situation $situation
     * @return Geo
     */
    public function addSituation(Situation $situation)
    {
        $situation->setGeo($this);
        $this->situations[] = $situation;

        return $this;
    }

    /**
     * Remove situations
     *
     * @param Situation $situation
     */
    public function removeSituation(Situation $situation)
    {
        $this->situations->removeElement($situation);
    }

    /**
     * Get situations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSituations()
    {
        $situations = $this->situations->getValues();
        uasort($situations, function(Situation $a, Situation $b) {
            if ($a->getDate() == $b->getDate()) {
                return 0;
            }
            return ($a->getDate() > $b->getDate()) ? -1 : 1;
        });
        return $situations;
    }

    /**
     * Get situations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMessages()
    {
        $messages = $this->messages->getValues();
        uasort($messages, function(MessageBoard $a, MessageBoard $b) {
            if ($a->getDate() == $b->getDate()) {
                return 0;
            }
            return ($a->getDate() > $b->getDate()) ? -1 : 1;
        });
        return $messages;
    }

    /**
     * Add aidsNeeded
     *
     * @param AidNeeded $need
     * @return Geo
     */
    public function addNeed(AidNeeded $need)
    {
        $need->setGeo($this);
        $this->aidsNeeded[] = $need;

        return $this;
    }

    /**
     * Remove aidsNeeded
     *
     * @param AidNeeded $need
     */
    public function removeNeed(AidNeeded $need)
    {
        $this->aidsNeeded->removeElement($need);
    }

    /**
     * Get aidsNeeded
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAidsNeeded()
    {
        return $this->aidsNeeded;
    }

    /**
     * Add aid
     *
     * @param Aid $aid
     * @return Geo
     */
    public function addAid(Aid $aid)
    {
        $this->aids[] = $aid;

        return $this;
    }

    /**
     * Remove aid
     *
     * @param Aid $aid
     */
    public function removeAid(Aid $aid)
    {
        $this->aids->removeElement($aid);
    }

    /**
     * Get aids
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAids()
    {
        return $this->sortAids($this->aids->getValues());
    }
    
    public function __toString()
    {
        return $this->id;
    }
    
    public abstract function getOptionLevel($text);
    
    public function getOptionName()
    {
        return $this->getOptionLevel('. ').' '.$this->__toString();
    }

    /**
     * Set population
     *
     * @param float $population
     * @return Barangay
     */
    public function setPopulation($population)
    {
        $this->population = $population;

        return $this;
    }

    /**
     * Get population
     *
     * @return float 
     */
    public function getPopulation()
    {
        return $this->population;
    }

    /**
     * Set estimatedHouseholds
     *
     * @param integer $estimatedHouseholds
     * @return Barangay
     */
    public function setEstimatedHouseholds($estimatedHouseholds)
    {
        $this->estimatedHouseholds = $estimatedHouseholds;

        return $this;
    }

    /**
     * Get estimatedHouseholds
     *
     * @return integer 
     */
    public function getEstimatedHouseholds()
    {
        return $this->estimatedHouseholds;
    }

    /**
     * Set affectedHouseholds
     *
     * @param integer $affectedHouseholds
     * @return Barangay
     */
    public function setAffectedHouseholds($affectedHouseholds)
    {
        $this->affectedHouseholds = $affectedHouseholds;

        return $this;
    }

    /**
     * Get affectedHouseholds
     *
     * @return integer 
     */
    public function getAffectedHouseholds()
    {
        return $this->affectedHouseholds;
    }
    
    public abstract function hasActivities(AidManager $mgr);
 
    public function lastReport()
    {
        if (count($this->aids) > 0) {
            return $this->aids->last();
        }
    }
    
    public function isProvince()
    {
        return $this instanceof Province;
    }
    
    public function isMunicipality()
    {
        return $this instanceof Municipality;
    }
    
    public function isBarangay()
    {
        return $this instanceof Barangay;
    }
    
    public function getActiveNgos()
    {
        $result = array();
        foreach ($this->aids->getValues() as $aid) {
            $ngo = $aid->getNgo();
            $result[$ngo->getId()] = $ngo;
        }
        return $result;
    }
    
    protected function sortAids($aids)
    {
        $sorter = function(Aid $a, Aid $b) {
            if ($a->getDateStart() == $b->getDateStart()) {
                return 0;
            }
            return ($a->getDateStart() > $b->getDateStart()) ? -1 : 1;
        };
        uasort($aids, $sorter);
        return $aids;
    }

    /**
     * @return integer
     */
    public abstract function getMapZoom();
    
    public function getLatitude()
    {
        $coords = $this->getCoords();
        if (!$coords || count($coords) == 0) {
            return '10.633614991656064';
        }
        $l = array();
        foreach ($coords as $c) $l[] = $c->lat;
        return array_sum($l)/count($l);
    }
    
    public function getLongitude()
    {
        $coords = $this->getCoords();
        if (!$coords || count($coords) == 0) {
            return '123.93951416015626';
        }
        $l = array();
        foreach ($coords as $c) $l[] = $c->lng;
        return array_sum($l)/count($l);
    }
    
    public function getCoords()
    {
        if ($this->map) {
            $coords = array();
            $latlng = explode(';', $this->map);
            foreach ($latlng as $ll) {
                list($lat,$lng) = explode(',', $ll);
                $coords[] = (object) array('lat' => $lat, 'lng' => $lng);
            }
            return $coords;
        }
    }
}
