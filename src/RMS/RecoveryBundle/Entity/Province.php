<?php

namespace RMS\RecoveryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RMS\RecoveryBundle\Model\AidManager;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Province
 *
 * @ORM\Table(name="geo_province")
 * @ORM\Entity()
 * @UniqueEntity(fields={"name"})
 */
class Province extends Geo
{

    /**
     * @ORM\OneToMany(targetEntity="Municipality", mappedBy="province", cascade={"persist"})
     *
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $municipalities;

    /**
     * @ORM\OneToMany(targetEntity="HealthLocation", mappedBy="province", cascade={"persist"})
     *
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $healthLocations;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->municipalities = new ArrayCollection();
    }

    /**
     * Add municipalities
     *
     * @param Municipality $municipality
     * @return Province
     */
    public function addMunicipalitie(Municipality $municipality)
    {
        $this->municipalities[] = $municipality;

        return $this;
    }

    /**
     * Remove municipalities
     *
     * @param Municipality $municipality
     */
    public function removeMunicipalitie(Municipality $municipality)
    {
        $this->municipalities->removeElement($municipality);
    }

    /**
     * Get municipalities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMunicipalities()
    {
        $municipalities = $this->municipalities->getValues();
        uasort($municipalities, function($a, $b) {
            if ($a->getName() == $b->getName()) {
                return 0;
            }
            return ($a->getName() < $b->getName()) ? -1 : 1;
        });
        return $municipalities;
    }

    public function __toString()
    {
        return $this->name.'';
    }

    public function hasActivities(AidManager $mgr)
    {
        if ($mgr->hasAids($this)) {
            return true;
        }
        foreach ($this->municipalities->getValues() as $municipality) {
            if ($municipality->hasActivities($mgr)) {
                return true;
            }
        }
    }

    public function getOptionLevel($text)
    {
        return '';
    }

    /**
     * @return integer
     */
    public function getMapZoom()
    {
        return 8;
    }

    public function getHealthLocations()
    {
        return $this->healthLocations;
    }

    public function getTreeIds()
    {
		$result = array($this->id);
		foreach ($this->municipalities as $muni) {
			$result = array_merge($result, $muni->getTreeIds());
		}
		return $result;
    }

}
