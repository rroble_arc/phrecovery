<?php

namespace RMS\RecoveryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @author Dottie Decano<d.decano@arcanys.com>
 * @ORM\Table(name="project_location")
 * @ORM\Entity()
 */
class ProjectLocation
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     * @Assert\NotBlank()
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="Geo", cascade={"persist"}, inversedBy="aids")
     * @ORM\JoinColumn(name="geo_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     *
     * @var Geo
     */
    private $geo;

    /**
     * @ORM\OneToMany(targetEntity="Project", mappedBy="location", cascade={"persist"})
     *
     * @var \Doctrine\Common\Collections\Collection
     */
    private $projects;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ProjectLocation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set geo
     *
     * @param integer $geo
     * @return ProjectLocation
     */
    public function setGeo($geo)
    {
        $this->geo = $geo;

        return $this;
    }

    /**
     * Get geo
     *
     * @return integer
     */
    public function getGeo()
    {
        return $this->geo;
    }

    /**
     * Set projects
     *
     * @param integer $projects
     * @return ProjectLocation
     */
    public function setProjects($projects)
    {
        $this->projects = $projects;

        return $this;
    }

    /**
     * Get projects
     *
     * @return integer
     */
    public function getProjects()
    {
        return $this->projects;
    }
}
