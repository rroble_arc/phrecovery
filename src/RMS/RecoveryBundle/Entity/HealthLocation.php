<?php

namespace RMS\RecoveryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 * @ORM\Table(name="health_location", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="unique_name_idx", columns={"name", "province_id"})
 * })
 * @ORM\Entity()
 * @UniqueEntity(fields={"name", "province"})
 */
class HealthLocation
{

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     * @Assert\NotBlank()
     *
     * @var string
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="Province", cascade={"persist"}, inversedBy="healthLocations")
     * @ORM\JoinColumn(name="province_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     *
     * @var Province
     */
    protected $province;

    /**
     * @ORM\OneToMany(targetEntity="HealthTeam", mappedBy="location", cascade={"persist"})
     *
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $healthTeams;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return HealthTeam
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set province
     *
     * @param Province $province
     * @return HealthTeam
     */
    public function setProvince(Province $province)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return Province
     */
    public function getProvince()
    {
        return $this->province;
    }

    public function __toString()
    {
        return $this->name.'';
    }

    public function getHealthTeams()
    {
        return $this->healthTeams;
    }

    public function getLocationName()
    {
        return $this->province.' - '. $this->name;
    }

}
