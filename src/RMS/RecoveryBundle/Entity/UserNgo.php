<?php

namespace RMS\RecoveryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * UserNgo
 *
 * @ORM\Table(name="user_ngo")
 * @ORM\Entity()
 */
class UserNgo extends User
{

    /**
     * @ORM\Column(name="title", type="string", length=45, nullable=true)
     * 
     * @var string
     */
    protected $title;

    /**
     * @ORM\Column(name="contact_info", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $contactInfo;

    /**
     * @ORM\ManyToOne(targetEntity="Ngo", cascade={"persist"}, inversedBy="users")
     * @ORM\JoinColumn(name="ngo_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * 
     * @var Ngo
     */
    protected $ngo;

    /**
     * @ORM\Column(name="is_coordinator", type="boolean", nullable=true)
     * 
     * @var boolean
     */
    protected $coordinator = true;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->addRole(static::ROLE_NGO);
    }

    /**
     * Set title
     *
     * @param string $title
     * @return UserNgo
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set contactInfo
     *
     * @param string $contactInfo
     * @return UserNgo
     */
    public function setContactInfo($contactInfo)
    {
        $this->contactInfo = $contactInfo;

        return $this;
    }

    /**
     * Get contactInfo
     *
     * @return string 
     */
    public function getContactInfo()
    {
        return $this->contactInfo;
    }

    /**
     * Set ngo
     *
     * @param Ngo $ngo
     * @return UserNgo
     */
    public function setNgo(Ngo $ngo)
    {
        $this->ngo = $ngo;

        return $this;
    }

    /**
     * Get ngo
     *
     * @return Ngo 
     */
    public function getNgo()
    {
        return $this->ngo;
    }
    
    /**
     * @return boolean
     */
    public function isVerified()
    {
        return $this->getNgo()->isVerified();
    }
    
    /**
     * @return boolean
     */
    public function isCoordinator()
    {
        return (bool) $this->coordinator;
    }

    /**
     * @param boolean $coordinator
     */
    public function setCoordinator($coordinator)
    {
        $this->coordinator = $coordinator;
    }

}
