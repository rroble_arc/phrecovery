<?php

namespace RMS\RecoveryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Help Center Response
 *
 * @ORM\Table(name="help_center_response")
 * @ORM\Entity
 */
class HelpCenterResponse extends AbstractTimestamptable implements Timestamptable
{    
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserNgo", inversedBy="reportedSituations", cascade={"persist"})
     * @ORM\JoinColumn(name="reporter", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * 
     * @var UserNgo
     */
    protected $reporter;

     /**
     * @ORM\ManyToOne(targetEntity="HelpCenter", inversedBy="responses")
     * @ORM\JoinColumn(name="help_center_id", referencedColumnName="id")
     *
     * @var \Doctrine\Common\Collections\Collection
     **/
    protected $post;   
    
    /**
     * @ORM\Column(name="message", type="text", nullable=true)
     * 
     * @var string
     */
    protected $message;
   
    public function __construct()
    {     
    
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reporter
     *
     * @param UserNgo $reporter
     * @return HelpCenterResponse
     */
    public function setReporter(UserNgo $reporter)
    {
        $this->reporter = $reporter;

        return $this;
    }

    /**
     * Get reporter
     *
     * @return HelpCenterResponse 
     */
    public function getReporter()
    {
        return $this->reporter;
    }

    /**
     * Set post
     *
     * @param HelpCenter $post
     * @return HelpCenterResponse 
     */
    public function setPost(HelpCenter $post)
    {
        $this->post = $post;
        
        return $this;
    }

    /**
     * Get post
     *
     * @param HelpCenter $post
     * @return HelpCenter
     */
    public function getPost()
    {
        return $this->post;
    }
    
    /**
     * Set message
     *
     * @param string $message
     * @return HelpCenter
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }
    
    public function getDate()
    {
        return $this->dateCreated;
    }
    
    public function __toString()
    {
        return $this->message.'';
    }
}
