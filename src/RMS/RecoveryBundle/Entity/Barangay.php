<?php

namespace RMS\RecoveryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RMS\RecoveryBundle\Model\AidManager;

/**
 * Barangay
 *
 * @ORM\Table(name="geo_barangay")
 * @ORM\Entity
 */
class Barangay extends Geo
{

    /**
     * @ORM\ManyToOne(targetEntity="Municipality", inversedBy="barangays", cascade={"persist"})
     * @ORM\JoinColumn(name="municipality_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * 
     * @var Municipality
     */
    protected $municipality;

    /**
     * Set municipality
     *
     * @param Municipality $municipality
     * @return Barangay
     */
    public function setMunicipality(Municipality $municipality = null)
    {
        $this->municipality = $municipality;

        return $this;
    }

    /**
     * Get municipality
     *
     * @return Municipality 
     */
    public function getMunicipality()
    {
        return $this->municipality;
    }
    
    public function __toString()
    {
        return $this->name.'';
    }

    public function hasActivities(AidManager $mgr)
    {
        return $mgr->hasAids($this);
    }

    public function getOptionLevel($text)
    {
        return str_repeat($text, 4);
    }

    /**
     * @return integer
     */
    public function getMapZoom()
    {
        if (!$this->map) {
            return 8;
        }
        return 14;
    }
    
}
