<?php

namespace RMS\RecoveryBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Dottie Decano <d.decano@arcanys.com>
 * @ORM\Table(name="Project")
 * @ORM\Entity()
 */
class Project
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
	 *
	 * @var integer
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Ngo", cascade={"persist"})
     * @ORM\JoinColumn(name="ngo_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     *
     * @var Ngo
     */
    private $ngo;

    /**
     * @ORM\ManyToOne(targetEntity="Geo", cascade={"persist"})
     * @ORM\JoinColumn(name="geo_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     *
     * @var Geo
     */
    protected $geo;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Assert\Length(max=400)
     *
     * @var string
     */
    protected $description;

    /**
     * @ORM\Column(name="date_start", type="date", nullable=false)
     *
     * @var \DateTime
     */
    protected $dateStart;

    /**
     * @ORM\Column(name="date_end", type="date", nullable=false)
     *
     * @var \DateTime
     */
    protected $dateEnd;

    /**
     * @ORM\Column(name="people_required", type="integer", nullable=true)
     *
     * @var integer
     */
    protected $peopleRequired;

    /**
     * @ORM\Column(name="skills", type="text", nullable=true)
     * @Assert\Length(max=100)
     *
     * @var string
     */
    protected $skills;

    /**
     * @ORM\ManyToOne(targetEntity="ProjectType", cascade={"persist"})
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     *
     * @var ProjectType
     */
    protected $type;

    /**
     * @ORM\Column(name="contact_instructions", type="text", nullable=true)
     * @Assert\Length(max=200)
     *
     * @var string
     */
    protected $contactInstructions;

    /**
     * @ORM\Column(name="contact_person", type="string", length=255, nullable=true)
     *
     * @var string
     */
    protected $contactPerson;

    /**
     * @ORM\Column(name="contact_number", type="string", length=255, nullable=true)
     *
     * @var string
     */
    protected $contactNumber;

    /**
     * @ORM\Column(name="email", type="string", length=45, nullable=true)
     * @Assert\Email()
     *
     * @var string
     */
    protected $email;

    /**
     * @ORM\Column(name="map", type="text", nullable=true)
     *
     * @var string
     */
    protected $map;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ngo
     *
     * @param Ngo $ngo
     * @return Project
     */
    public function setNgo(Ngo $ngo)
    {
        $this->ngo = $ngo;

        return $this;
    }

    /**
     * Get ngo
     *
     * @return Ngo
     */
    public function getNgo()
    {
        return $this->ngo;
    }

    /**
     * Set geo
     *
     * @param Geo $geo
     * @return Project
     */
    public function setGeo(Geo $geo)
    {
        $this->geo = $geo;

        return $this;
    }

    /**
     * Get geo
     *
     * @return Geo
     */
    public function getGeo()
    {
        return $this->geo;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     * @return Project
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     * @return Project
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set peopleRequired
     *
     * @param integer $peopleRequired
     * @return Project
     */
    public function setPeopleRequired($peopleRequired)
    {
        $this->peopleRequired = $peopleRequired;

        return $this;
    }

    /**
     * Get peopleRequired
     *
     * @return integer
     */
    public function getPeopleRequired()
    {
        return $this->peopleRequired;
    }

    /**
     * Set skills
     *
     * @param string $skills
     * @return Project
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;

        return $this;
    }

    /**
     * Get skills
     *
     * @return string
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * Set type
     *
     * @param ProjectType $type
     * @return Project
     */
    public function setType(ProjectType $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return ProjectType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set contactInstructions
     *
     * @param string $contactInstructions
     * @return Project
     */
    public function setContactInstructions($contactInstructions)
    {
        $this->contactInstructions = $contactInstructions;

        return $this;
    }

    /**
     * Get contactInstructions
     *
     * @return string
     */
    public function getContactInstructions()
    {
        return $this->contactInstructions;
    }

    /**
     * Set contactPerson
     *
     * @param string $contactPerson
     * @return Project
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * Get contactPerson
     *
     * @return string
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Set contactNumber
     *
     * @param string $contactNumber
     * @return Project
     */
    public function setContactNumber($contactNumber)
    {
        $this->contactNumber = $contactNumber;

        return $this;
    }

    /**
     * Get contactNumber
     *
     * @return string
     */
    public function getContactNumber()
    {
        return $this->contactNumber;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Project
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return Province
     */
    public function getProvince()
    {
        if ($this->geo) {
            if ($this->geo->isProvince()) {
                return $this->geo;
            } else if (($mun = $this->getMunicipality())) {
                return $mun->getProvince();
            } else if (($bar = $this->getBarangay())) {
                return $bar->getMunicipality()->getProvince();
            }
        }
    }

    public function setProvince(Province $province)
    {
        return $this;
    }

    /**
     * @return Municipality
     */
    public function getMunicipality()
    {
        if ($this->geo) {
            if ($this->geo->isMunicipality()) {
                return $this->geo;
            } else if (($bar = $this->getBarangay())) {
                return $bar->getMunicipality();
            }
        }
    }

    public function setMunicipality(Municipality $municipality)
    {
        return $this;
    }

    /**
     * @return Barangay
     */
    public function getBarangay()
    {
        if ($this->geo && $this->geo->isBarangay()) {
            return $this->geo;
        }
    }

	public function belongsToNgo(Ngo $ngo)
	{
		if($ngo->getId() == $this->getNgo()->getId())
		{
			return true;
		}
		return false;
	}

    /**
     * Set map
     *
     * @param string $map
     * @return Project
     */
    public function setMap($map)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * Get map
     *
     * @return string
     */
    public function getMap()
    {
        return $this->map;
    }

    public function getLatitude()
    {
        $coords = $this->getCoords();
        if (!$coords || count($coords) == 0) {
            return '10.633614991656064';
        }
        $l = array();
        foreach ($coords as $c) $l[] = $c->lat;
        return array_sum($l)/count($l);
    }

	/**
     * @return integer
     */
    public function getMapZoom()
	{
		return 8;
	}

    public function getLongitude()
    {
        $coords = $this->getCoords();
        if (!$coords || count($coords) == 0) {
            return '123.93951416015626';
        }
        $l = array();
        foreach ($coords as $c) $l[] = $c->lng;
        return array_sum($l)/count($l);
    }

    public function getCoords()
    {
        if ($this->map) {
            $coords = array();
            $latlng = explode(';', $this->map);
            foreach ($latlng as $ll) {
                list($lat,$lng) = explode(',', $ll);
                $coords[] = (object) array('lat' => $lat, 'lng' => $lng);
            }
            return $coords;
        }
    }

    public function __toString()
    {
        if (isset($this->description[50])) {
            return substr($this->description, 0, 50). '...';
        }
        return $this->description.'';
    }
}
