<?php

namespace RMS\RecoveryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RMS\RecoveryBundle\Model\AidManager;

/**
 * Municipality
 *
 * @ORM\Table(name="geo_municipality")
 * @ORM\Entity
 */
class Municipality extends Geo
{

    /**
     * @ORM\ManyToOne(targetEntity="Province", inversedBy="municipalities", cascade={"persist"})
     * @ORM\JoinColumn(name="province_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     *
     * @var Province
     */
    protected $province;

    /**
     * @ORM\OneToMany(targetEntity="Barangay", mappedBy="municipality", cascade={"persist"})
     *
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $barangays;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->barangays = new ArrayCollection();
    }

    /**
     * Set province
     *
     * @param Province $province
     * @return Municipality
     */
    public function setProvince(Province $province = null)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return Province
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Add barangays
     *
     * @param Barangay $barangay
     * @return Municipality
     */
    public function addBarangay(Barangay $barangay)
    {
        $barangay->setMunicipality($this);
        $this->barangays[] = $barangay;

        return $this;
    }

    /**
     * Remove barangays
     *
     * @param Barangay $barangay
     */
    public function removeBarangay(Barangay $barangay)
    {
        $this->barangays->removeElement($barangay);
    }

    /**
     * Get barangays
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBarangays()
    {
        $barangays = $this->barangays->getValues();
        uasort($barangays, function($a, $b) {
            if ($a->getName() == $b->getName()) {
                return 0;
            }
            return ($a->getName() < $b->getName()) ? -1 : 1;
        });
        return $barangays;
    }

    public function __toString()
    {
        return $this->name.'';
    }

    public function hasActivities(AidManager $mgr)
    {
        if ($mgr->hasAids($this)) {
            return true;
        }
        foreach ($this->barangays->getValues() as $barangay) {
            if ($barangay->hasActivities($mgr)) {
                return true;
            }
        }
    }

    public function getOptionLevel($text)
    {
        return str_repeat($text, 2);
    }

    public function getActiveNgos()
    {
        $result = parent::getActiveNgos();
        foreach ($this->barangays->getValues() as $barangay) {
            $activeNgos = $barangay->getActiveNgos();
            foreach ($activeNgos as $ngoId => $ngo) {
                $result[$ngoId] = $ngo;
            }
        }
        return $result;
    }

    /**
     * Get aids
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAids()
    {
        $aids = $this->aids->getValues();
        foreach ($this->barangays->getValues() as $barangay) {
            $aids = array_merge($aids, $barangay->getAids());
        }
        return $this->sortAids($aids);
    }

    /**
     * @return integer
     */
    public function getMapZoom()
    {
        if (!$this->map) {
            return 8;
        }
        return 11;
    }

	public function getTreeIds()
	{
		$result = array($this->id);
		foreach ($this->barangays as $bara) {
			$result[] = $bara->getId();
		}
		return $result;
	}

}
