<?php

namespace RMS\RecoveryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Dottie Decano <d.decano@arcanys.com>
 * @ORM\Table(name="health_team")
 * @ORM\Entity()
 */
class HealthTeam
{

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     * @Assert\NotBlank()
     * 
     * @var string
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="Ngo", cascade={"persist"}, inversedBy="healthTeams")
     * @ORM\JoinColumn(name="ngo_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     * 
     * @var Ngo
     */
    protected $ngo;

    /**
     * @ORM\ManyToOne(targetEntity="HealthLocation", cascade={"persist"}, inversedBy="healthTeams")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * 
     * @var HealthLocation
     */
    protected $location;

    /**
     * @ORM\Column(name="md", type="integer", nullable=true)
     * 
     * @var integer
     */
    protected $md;

    /**
     * @ORM\Column(name="rn", type="integer", nullable=true)
     * 
     * @var integer
     */
    protected $rn;

    /**
     * @ORM\Column(name="other", type="integer", nullable=true)
     * 
     * @var integer
     */
    protected $other;

    /**
     * @ORM\Column(name="skills", type="simple_array", nullable=true)
     * 
     * @var array
     */
    protected $skills;

    /**
     * @ORM\Column(name="services_rendered", type="string", length=150, nullable=true)
     * 
     * @var string
     */
    protected $servicesRendered;

    /**
     * @ORM\Column(name="date_arrived", type="date", nullable=true)
     * 
     * @var \DateTime
     */
    protected $dateArrived;

    /**
     * @ORM\Column(name="expected_departure", type="date", nullable=true)
     * 
     * @var \DateTime
     */
    protected $expectedDeparture;

    /**
     * @ORM\Column(name="contact_person", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $contactPerson;

    /**
     * @ORM\Column(name="contact_number", type="string", length=255, nullable=true)
     * 
     * @var string
     */
    protected $contactNumber;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return HealthTeam
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set md
     *
     * @param integer $md
     * @return HealthTeam
     */
    public function setMd($md)
    {
        $this->md = $md;

        return $this;
    }

    /**
     * Get md
     *
     * @return integer 
     */
    public function getMd()
    {
        return $this->md;
    }

    /**
     * Set rn
     *
     * @param integer $rn
     * @return HealthTeam
     */
    public function setRn($rn)
    {
        $this->rn = $rn;

        return $this;
    }

    /**
     * Get rn
     *
     * @return integer 
     */
    public function getRn()
    {
        return $this->rn;
    }

    /**
     * Set other
     *
     * @param integer $other
     * @return HealthTeam
     */
    public function setOther($other)
    {
        $this->other = $other;

        return $this;
    }

    /**
     * Get other
     *
     * @return integer 
     */
    public function getOther()
    {
        return $this->other;
    }

    /**
     * Set skills
     *
     * @param array $skills
     * @return HealthTeam
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;

        return $this;
    }

    /**
     * Get skills
     *
     * @return array 
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * Set servicesRendered
     *
     * @param string $servicesRendered
     * @return HealthTeam
     */
    public function setServicesRendered($servicesRendered)
    {
        $this->servicesRendered = $servicesRendered;

        return $this;
    }

    /**
     * Get servicesRendered
     *
     * @return string 
     */
    public function getServicesRendered()
    {
        return $this->servicesRendered;
    }

    /**
     * Set dateArrived
     *
     * @param \DateTime $dateArrived
     * @return HealthTeam
     */
    public function setDateArrived($dateArrived)
    {
        $this->dateArrived = $dateArrived;

        return $this;
    }

    /**
     * Get dateArrived
     *
     * @return \DateTime 
     */
    public function getDateArrived()
    {
        return $this->dateArrived;
    }

    /**
     * Set expectedDeparture
     *
     * @param \DateTime $expectedDeparture
     * @return HealthTeam
     */
    public function setExpectedDeparture($expectedDeparture)
    {
        $this->expectedDeparture = $expectedDeparture;

        return $this;
    }

    /**
     * Get expectedDeparture
     *
     * @return \DateTime 
     */
    public function getExpectedDeparture()
    {
        return $this->expectedDeparture;
    }

    /**
     * Set contactPerson
     *
     * @param string $contactPerson
     * @return HealthTeam
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * Get contactPerson
     *
     * @return string 
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Set contactNumber
     *
     * @param string $contactNumber
     * @return HealthTeam
     */
    public function setContactNumber($contactNumber)
    {
        $this->contactNumber = $contactNumber;

        return $this;
    }

    /**
     * Get contactNumber
     *
     * @return string 
     */
    public function getContactNumber()
    {
        return $this->contactNumber;
    }

    /**
     * Set ngo
     *
     * @param Ngo $ngo
     * @return HealthTeam
     */
    public function setNgo(Ngo $ngo)
    {
        $this->ngo = $ngo;

        return $this;
    }

    /**
     * Get ngo
     *
     * @return Ngo 
     */
    public function getNgo()
    {
        return $this->ngo;
    }

    /**
     * Set location
     *
     * @param HealthLocation $location
     * @return HealthTeam
     */
    public function setLocation(HealthLocation $location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return HealthLocation 
     */
    public function getLocation()
    {
        return $this->location;
    }

    public function __toString()
    {
        return $this->name.'';
    }

	public function getTotal()
	{
		return $this->md + $this->rn + $this->other; 
	}
}
