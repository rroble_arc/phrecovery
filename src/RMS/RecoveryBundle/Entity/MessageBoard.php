<?php

namespace RMS\RecoveryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Situation
 *
 * @ORM\Table(name="message_board")
 * @ORM\Entity
 */
class MessageBoard extends AbstractTimestamptable implements Timestamptable
{

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserNgo", inversedBy="reportedSituations", cascade={"persist"})
     * @ORM\JoinColumn(name="reporter", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * 
     * @var UserNgo
     */
    protected $reporter;

    /**
     * @ORM\ManyToOne(targetEntity="Geo", inversedBy="messages", cascade={"persist"})
     * @ORM\JoinColumn(name="geo_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * 
     * @var Geo
     */
    protected $geo;

    /**
     * @ORM\Column(name="message", type="text", nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max=200)
     * 
     * @var string
     */
    protected $message;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reporter
     *
     * @param UserNgo $reporter
     * @return Situation
     */
    public function setReporter(UserNgo $reporter)
    {
        $this->reporter = $reporter;

        return $this;
    }

    /**
     * Get reporter
     *
     * @return UserNgo 
     */
    public function getReporter()
    {
        return $this->reporter;
    }

    /**
     * Set geo
     *
     * @param Geo $geo
     * @return Situation
     */
    public function setGeo(Geo $geo)
    {
        $this->geo = $geo;

        return $this;
    }

    /**
     * Get geo
     *
     * @return Geo 
     */
    public function getGeo()
    {
        return $this->geo;
    }

    /**
     * Set message
     *
     * @param Message $message
     * @return Situation
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return Message 
     */
    public function getMessage()
    {
        return $this->message;
    }

    public function getDate()
    {
        return $this->dateCreated;
    }
    
    public function __toString()
    {
        return $this->message.'';
    }
    
}
