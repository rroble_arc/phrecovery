<?php

namespace RMS\RecoveryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AidNeeded
 *
 * @ORM\Table(name="aid_needed")
 * @ORM\Entity
 */
class AidNeeded extends AbstractTimestamptable implements Timestamptable
{

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="AidType", inversedBy="aidsNeeded", cascade={"persist"})
     * @ORM\JoinTable(name="aid_needed_types",
     *      joinColumns={@ORM\JoinColumn(name="aid_needed_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="aid_type_id", referencedColumnName="id")}
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $aidTypes;
    
    /**
     * @ORM\Column(name="description", type="string", length=45, nullable=false)
     * @Assert\NotBlank()
     * 
     * @var string
     */
    protected $description;

    /**
     * @ORM\Column(name="status", type="string", length=45, nullable=true)
     * 
     * @var string
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="reportedNeeds", cascade={"persist"})
     * @ORM\JoinColumn(name="reporter", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * 
     * @var User
     */
    protected $reporter;

    /**
     * @ORM\ManyToOne(targetEntity="Geo", inversedBy="aidsNeeded", cascade={"persist"})
     * @ORM\JoinColumn(name="geo_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * 
     * @var Geo
     */
    protected $geo;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        
    }

    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return AidNeeded
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return AidNeeded
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set reporter
     *
     * @param User $reporter
     * @return AidNeeded
     */
    public function setReporter(User $reporter)
    {
        $this->reporter = $reporter;

        return $this;
    }

    /**
     * Get reporter
     *
     * @return User 
     */
    public function getReporter()
    {
        return $this->reporter;
    }

    /**
     * Set geo
     *
     * @param Geo $geo
     * @return AidNeeded
     */
    public function setGeo(Geo $geo)
    {
        $this->geo = $geo;

        return $this;
    }

    /**
     * Get geo
     *
     * @return Geo 
     */
    public function getGeo()
    {
        return $this->geo;
    }
    
    /**
     * Add type
     *
     * @param AidType $type
     * @return AidNeeded
     */
    public function addAidType(AidType $type)
    {        
        $this->aidTypes[] = $type;

        return $this;
    }

    /**
     * Remove type
     *
     * @param AidType $type
     */
    public function removeAidType(AidType $type)
    {
        $this->aidTypes->removeElement($type);
    }
    
    /**
     * Get aid types
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAidTypes()
    {
        return $this->aidTypes;
    }
    
    public function __toString()
    {
        return $this->description.'';
    }

    public function getDate()
    {
        return $this->dateCreated;
    }
    
    /**
     * Get aid types
     *
     * @return string
     */
    public function getTypes()
    {
        $types = array();
        foreach ($this->aidTypes->getValues() as $aidType) {
            $types[] = $aidType->getName();
        }
        return implode(', ', $types);;
    }
    
}
