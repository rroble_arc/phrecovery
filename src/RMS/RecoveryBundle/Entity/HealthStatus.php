<?php

namespace RMS\RecoveryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 * @ORM\Table(name="health_status")
 * @ORM\Entity()
 */
class HealthStatus extends AbstractTimestamptable
{

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Ngo", inversedBy="reportedMessages", cascade={"persist"})
     * @ORM\JoinColumn(name="ngo_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * 
     * @var Ngo
     */
    protected $reporter;

    /**
     * @ORM\ManyToOne(targetEntity="HealthLocation", inversedBy="messages", cascade={"persist"})
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * 
     * @var HealthLocation
     */
    protected $location;

    /**
     * @ORM\Column(name="message", type="text", nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max=200)
     * 
     * @var string
     */
    protected $message;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Ngo
     */
    public function getReporter()
    {
        return $this->reporter;
    }

    public function setReporter(Ngo $reporter)
    {
        $this->reporter = $reporter;
    }

    /**
     * @return HealthLocation
     */
    public function getLocation()
    {
        return $this->location;
    }

    public function setLocation(HealthLocation $location)
    {
        $this->location = $location;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }
    
    public function __toString()
    {
        if (isset($this->message[50])) {
            return substr($this->message, 0, 50).' ...';
        }
        return $this->message.'';
    }

    public function getDate()
    {
        return $this->dateCreated;
    }
    
}
