<?php

namespace RMS\RecoveryBundle\Command;

use RMS\RecoveryBundle\Model\CsvReader;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class ImportGeoMapCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('rms:geomap:import');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $bundle = $this->getApplication()->getKernel()->getBundle('RMSRecoveryBundle');
        $file = $bundle->getPath() . '/Resources/data/philippine_barangay_longitude_latitude.csv';
        
        $em = $this->getContainer()->get('doctrine')->getManager();
        $reader = new CsvReader($file);

        $reader->setIgnoreLine(1);
        $geodata = $reader->read();

        set_time_limit(0);
        ini_set('memory_limit', '512M');
        
        $provinces = array();
        $gp = array();
        $municipalities = array();
        $gm = array();
        
        $output->writeln('Importing geo map data...');
        foreach ($geodata as $row) {
            $long = $row[3]; $lat = $row[4];
            $province = $this->getProvince($this->toName($row[0]));
            $municipality = $this->getMunicipality($this->toName($row[1]), $province);
            $barangay = $this->getBarangay($this->toName($row[2]), $municipality);
            if ($barangay) {
                if (!$barangay->getMap()) {
                    $output->writeln($province->getName().' : '
                            . $municipality->getName().' : '
                            . $barangay->getName());
                    $map = implode(',', array($lat, $long));
                    $barangay->setMap($map);
                    $em->persist($barangay);
                }
                $provinces[$province->getId()] = $province;
                $gp[$province->getId()][] = array($lat, $long);
                $municipalities[$municipality->getId()] = $municipality;
                $gm[$municipality->getId()][] = array($lat, $long);
            }
        }
        $output->writeln(PHP_EOL.'.. Saving');
        $output->writeln('Municipalities..');
        foreach ($gm as $mid => $ll) {
            $muni = $municipalities[$mid];
            if ($muni->getMap()) continue;
            $lats = array();
            $longs = array();
            foreach ($ll as $l) {
                $lats[] = $l[0];
                $longs[] = $l[1];
            }
            $lata = array_sum($lats) / count($lats);
            $longa = array_sum($longs) / count($longs);
            $map = implode(',', array($lata, $longa));
            $muni->setMap($map);
            $em->persist($muni);
        }
        
        $output->writeln('Provinces..');
        foreach ($gp as $pid => $ll) {
            $prov = $provinces[$pid];
            if ($prov->getMap()) continue;
            $lats = array();
            $longs = array();
            foreach ($ll as $l) {
                $lats[] = $l[0];
                $longs[] = $l[1];
            }
            $lata = array_sum($lats) / count($lats);
            $longa = array_sum($longs) / count($longs);
            $map = implode(',', array($lata, $longa));
            $prov->setMap($map);
            $em->persist($prov);
        }
        
        $em->flush();
        $output->writeln('Done');
    }

    private function getBarangay($name, $municipality)
    {
        static $list = array();
        if (!$name || !$municipality) return;
        $key = $municipality->getId().':'.$name;
        if (isset($list[$key])) {
            return $list[$key];
        }
        $em = $this->getContainer()->get('doctrine')->getManager();
        $entities = $em->getRepository('RMSRecoveryBundle:Barangay')
                ->findBy(array('name' => $name));
        foreach ((array) $entities as $entity) {
            if ($entity->getMunicipality()->getId() == $municipality->getId()) {
                $list[$key] = $entity;
                return $entity;
            }
        }
    }
    
    private function getMunicipality($name, $province)
    {
        static $list = array();
        if (!$name || !$province) return;
        $key = $province->getId().':'.$name;
        if (isset($list[$key])) {
            return $list[$key];
        }
        $em = $this->getContainer()->get('doctrine')->getManager();
        $entities = $em->getRepository('RMSRecoveryBundle:Municipality')
                ->findBy(array('name' => $name));
        foreach ((array) $entities as $entity) {
            if ($entity->getProvince()->getId() == $province->getId()) {
                $list[$key] = $entity;
                return $entity;
            }
        }
    }

    private function getProvince($name)
    {
        static $list = array();
        if (!$name) return;
        if (isset($list[$name])) {
            return $list[$name];
        }
        $em = $this->getContainer()->get('doctrine')->getManager();
        $entity = $em->getRepository('RMSRecoveryBundle:Province')
                ->findOneBy(array('name' => $name));
        if ($entity) {
            $list[$name] = $entity;
            return $entity;
        }
    }
    
    private function toName($name)
    {
        return ucwords(strtolower($name));
    }

}
