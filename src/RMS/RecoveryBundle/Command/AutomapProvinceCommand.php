<?php

namespace RMS\RecoveryBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class AutomapProvinceCommand extends AutomapMunicipalityCommand
{

    protected function configure()
    {
        $this->setName('rms:province:automap');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* @var $em \Doctrine\Common\Persistence\ObjectManager */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $provinces = $em->getRepository('RMSRecoveryBundle:Province')->findAll();

        /* @var $province \RMS\RecoveryBundle\Entity\Province */
        /* @var $municipality \RMS\RecoveryBundle\Entity\Municipality */
        /* @var $barangay \RMS\RecoveryBundle\Entity\Barangay */
        
        foreach ($provinces as $province) {
            $points = array();
            foreach ($province->getMunicipalities() as $municipality) {
                $barangays = $municipality->getBarangays();
                foreach ($barangays as $barangay) {
                    $c = (array) $barangay->getCoords();
                    foreach ($c as $_c) {
                        $points[] = array($_c->lat, $_c->lng);
                    }
                }
            }
            if (!$points) continue;
            $map = implode(';', $this->getBoundary($points));
            $province->setMap($map);
            $em->persist($province);
        }
        $em->flush();
    }
    
}
