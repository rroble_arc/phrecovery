<?php

namespace RMS\RecoveryBundle\Command;

use RMS\RecoveryBundle\Entity\Aid;
use RMS\RecoveryBundle\Entity\AidType;
use RMS\RecoveryBundle\Entity\Barangay;
use RMS\RecoveryBundle\Entity\Municipality;
use RMS\RecoveryBundle\Entity\Ngo;
use RMS\RecoveryBundle\Entity\Province;
use RMS\RecoveryBundle\Model\CsvReader;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class ImportActivityCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('rms:aid:import');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $bundle = $this->getApplication()->getKernel()->getBundle('RMSRecoveryBundle');
        $file = $bundle->getPath() . '/Resources/data/Inventory_Nov14.csv';
        $reader = new CsvReader($file);
        $output->writeln('Importing aid acitivities...');
        $em = $this->getContainer()->get('doctrine')->getManager();
        
        $reader->setIgnoreLine(1);
        $geodata = $reader->read();
        
        $province = null;
        $municipality = null;
        $ngo = null;
        foreach ($geodata as $k => $row) {
            if ($row[0]) {
                $province = $this->getProvince($row[0]);
                $municipality = null;
            }
            if ($row[1]) {
                $municipality = $this->getMunicipality($row[1], $province);
            }
            if ($row[4]) {
                $ngo = $this->getNgo($row[4]);
            }
            if ($row[5]) {
                $type = $this->getType($row[5]);
                $aid = new Aid();
                $aid->setNgo($ngo);
                $aid->setType($type);
                $aid->setDescription($row[6]);
                $aid->setQuantity($row[7]);
                $aid->setUnitCost($row[8]);
                $aid->setPeopleCatered(0);
                $dt = new \DateTime();
                $dt->setDate(2013, 1, 1);
                $aid->setDateStart($dt);
                $aid->setGeo($municipality ? $municipality : $province);
                $em->persist($aid);
            }
        }
        
        $em->flush();
        
        $output->writeln(PHP_EOL.'Done');
    }
    
    private function getType($name)
    {
        static $list = array();
        if (!isset($list[$name])) {
            $em = $this->getContainer()->get('doctrine')->getManager();
            $entity = $em->getRepository('RMSRecoveryBundle:AidType')
                    ->findOneBy(array('name' => $name));
            if (!$entity) {
                $entity = new AidType();
                $entity->setName($name);
            }
            $em->persist($entity);
            $list[$name] = $entity;
        }
        return $list[$name];
    }
    
    private function getNgo($name)
    {
        static $list = array();
        if (!isset($list[$name])) {
            $em = $this->getContainer()->get('doctrine')->getManager();
            $entity = $em->getRepository('RMSRecoveryBundle:Ngo')
                    ->findOneBy(array('name' => $name));
            if (!$entity) {
                $entity = new Ngo();
                $entity->setName($name);
            }
            $em->persist($entity);
            $list[$name] = $entity;
        }
        return $list[$name];
    }
    
    private function getBarangay($name, $municipality, $population = null)
    {
        static $list = array();
        if (!isset($list[$name])) {
            $em = $this->getContainer()->get('doctrine')->getManager();
            $entity = $em->getRepository('RMSRecoveryBundle:Barangay')
                    ->findOneBy(array('name' => $name));
            if (!$entity) {
                $entity = new Barangay();
                $entity->setMunicipality($municipality);
                $entity->setName($name);
            }
            if ($population) {
                $entity->setPopulation($population);
            }
            $em->persist($entity);
            $list[$name] = $entity;
        }
        return $list[$name];
    }
    
    private function getMunicipality($name, $province, $population = null)
    {
        static $list = array();
        if (!isset($list[$name])) {
            $em = $this->getContainer()->get('doctrine')->getManager();
            $entity = $em->getRepository('RMSRecoveryBundle:Municipality')
                    ->findOneBy(array('name' => $name));
            if (!$entity) {
                $entity = new Municipality();
                $entity->setProvince($province);
                $entity->setName($name);
            }
            if ($population) {
                $entity->setPopulation($population);
            }
            $em->persist($entity);
            $list[$name] = $entity;
        }
        return $list[$name];
    }
    
    private function getProvince($name)
    {
        static $list = array();
        if (!isset($list[$name])) {
            $em = $this->getContainer()->get('doctrine')->getManager();
            $entity = $em->getRepository('RMSRecoveryBundle:Province')
                    ->findOneBy(array('name' => $name));
            if (!$entity) {
                $entity = new Province();
                $entity->setName($name);
                $em->persist($entity);
            }
            $list[$name] = $entity;
        }
        return $list[$name];
    }

}
