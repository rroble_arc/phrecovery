<?php

namespace RMS\RecoveryBundle\Command;

use RMS\RecoveryBundle\Model\ConvexHull;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class AutomapMunicipalityCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('rms:municipality:automap');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* @var $em \Doctrine\Common\Persistence\ObjectManager */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $municipalities = $em->getRepository('RMSRecoveryBundle:Municipality')->findAll();

        /* @var $municipality \RMS\RecoveryBundle\Entity\Municipality */
        /* @var $barangay \RMS\RecoveryBundle\Entity\Barangay */
        
        foreach ($municipalities as $municipality) {
            $barangays = $municipality->getBarangays();
            $points = array();
            foreach ($barangays as $barangay) {
                $c = (array) $barangay->getCoords();
                foreach ($c as $_c) {
                    $points[] = array($_c->lat, $_c->lng);
                }
            }
            if (!$points) continue;
            $map = implode(';', $this->getBoundary($points));
            $municipality->setMap($map);
            $em->persist($municipality);
        }
        $em->flush();
    }
    
    protected function getBoundary($points)
    {
        $h = new ConvexHull($points);
        $hp = $h->getHullPoints();
        $filteredCoords = array();
        foreach ($hp as $p) {
            $filteredCoords[] = (object) array(
                'lat' => $p[0],
                'lng' => $p[1],
            );
        }
        $sortedCoords = $this->sortCenter($filteredCoords);
        $newCoords = array();
        foreach ($sortedCoords as $fc) {
            $newCoords[] = implode(',', array($fc->lat, $fc->lng));
        }
        return $newCoords;
    }


    protected function sortCenter(array $coords)
    {
        $reference = $this->getCenter($coords);
        uasort($coords, function($a, $b) use($reference)  {
            $aTanA = atan2(($a->lng - $reference->lng),($a->lat - $reference->lat));
            $aTanB = atan2(($b->lng - $reference->lng),($b->lat - $reference->lat));
            if ($aTanA < $aTanB) return -1;
            else if ($aTanB < $aTanA) return 1;
            return 0;
        });
        return $coords;
    }
    
    protected function getCenter(array $coords)
    {
        $lats = array();
        $lngs = array();
        foreach ($coords as $coord) {
            $lats[] = $coord->lat;
            $lngs[] = $coord->lng;
        }
        $c = count($coords);
        return (object) array(
            'lat' => array_sum($lats)/$c,
            'lng' => array_sum($lngs)/$c,
        );
    }

}
