<?php

namespace RMS\RecoveryBundle\Command;

use RMS\RecoveryBundle\Entity\Barangay;
use RMS\RecoveryBundle\Entity\Municipality;
use RMS\RecoveryBundle\Entity\Province;
use RMS\RecoveryBundle\Model\CsvReader;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class ImportGeoDataCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('rms:geo:import');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $bundle = $this->getApplication()->getKernel()->getBundle('RMSRecoveryBundle');
        $path = $bundle->getPath() . '/Resources/data/Cities-Information';
        
        $finder = new Finder();
        $finder->files()->in($path);
        
        // 0 Province
        // 1 Municipality
        // 2 Barangay
        // 3 Population 
        // 4 Estimated households
        $output->writeln('Importing geo data...');
        
        foreach ($finder as $file) {
            $output->writeln($file);
            $reader = new CsvReader($file);
            $em = $this->getContainer()->get('doctrine')->getManager();

            $reader->setIgnoreLine(1);
            $geodata = $reader->read();

            $province = $municipality = null;
            
            foreach ($geodata as $k => $row) {
                $output->write('.');
                if ($k > 0 && $k % 100 == 0) $output->write(PHP_EOL);
                $prov = isset($row[0]) ? $row[0] : null;
                $mun = isset($row[1]) ? $row[1] : null;
                $bar = isset($row[2]) ? $row[2] : null;
                $pop = isset($row[3]) ? $row[3] : null;
                $hh = isset($row[4]) ? $row[4] : null;
                if ($prov) {
                    $province = $this->getProvince($prov);
                }
                if ($mun && $province) {
                    $municipality = $this->getMunicipality($mun, $province, $this->tonum($pop), $this->tonum($hh));
                }
                if ($bar && $municipality) {
                    $this->getBarangay($bar, $municipality, $this->tonum($pop), $this->tonum($hh));
                }
            }
            $output->writeln(PHP_EOL.'.. Saving');
            $em->flush();
        }
        
        $output->writeln('Done');
    }
    
    private function tonum($n)
    {
        if ($n) return preg_replace('#[^0-9.]#', '', $n);
        return $n;
    }

    private function getBarangay($name, $municipality, $population, $households = null)
    {
        static $list = array();
        if (!isset($list[$name])) {
            $em = $this->getContainer()->get('doctrine')->getManager();
            $entity = $em->getRepository('RMSRecoveryBundle:Barangay')
                    ->findOneBy(array('name' => $name));
            if (!$entity) {
                $entity = new Barangay();
                $entity->setMunicipality($municipality);
                $entity->setName($name);
            }
            if ($population)  $entity->setPopulation($population);
            if ($households)  $entity->setEstimatedHouseholds($households);
            $em->persist($entity);
            $list[$name] = $entity;
        }
        return $list[$name];
    }
    
    private function getMunicipality($name, $province, $population, $households = null)
    {
        static $list = array();
        if (!isset($list[$name])) {
            $em = $this->getContainer()->get('doctrine')->getManager();
            $entity = $em->getRepository('RMSRecoveryBundle:Municipality')
                    ->findOneBy(array('name' => $name));
            if (!$entity) {
                $entity = new Municipality();
                $entity->setProvince($province);
                $entity->setName($name);
            }
            if ($population)  $entity->setPopulation($population);
            if ($households)  $entity->setEstimatedHouseholds($households);
            $em->persist($entity);
            $list[$name] = $entity;
        }
        return $list[$name];
    }
    
    private function getProvince($name)
    {
        static $list = array();
        if (!isset($list[$name])) {
            $em = $this->getContainer()->get('doctrine')->getManager();
            $entity = $em->getRepository('RMSRecoveryBundle:Province')
                    ->findOneBy(array('name' => $name));
            if (!$entity) {
                $entity = new Province();
                $entity->setName($name);
                $em->persist($entity);
            }
            $list[$name] = $entity;
        }
        return $list[$name];
    }

}
