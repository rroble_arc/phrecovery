<?php

namespace RMS\RecoveryBundle\Command;

use RMS\RecoveryBundle\Entity\HealthLocation;
use RMS\RecoveryBundle\Entity\HealthTeam;
use RMS\RecoveryBundle\Model\CsvReader;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class ImportHealthTeamCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('rms:team:import');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $bundle = $this->getApplication()->getKernel()->getBundle('RMSRecoveryBundle');
        $file = $bundle->getPath() . '/Resources/data/HealthOrganizationsData.csv';
        $reader = new CsvReader($file);
        $output->writeln('Importing health teams...');
        $em = $this->getContainer()->get('doctrine')->getManager();
        
        $reader->setIgnoreLine(1);
        $geodata = $reader->read();
        
        foreach ($geodata as $k => $row) {
            $location = $this->getLocation($row[0]);
            $team = $this->getTeam($row[1]);
            $team->setLocation($location);
            $team->setMd($row[2]);
            $team->setRn($row[3]);
            $team->setOther($row[4]);
            if (trim($row[6])) {
                $team->setSkills(explode(',', $row[6]));
            }
            $team->setServicesRendered($row[7]);
            $team->setContactPerson($row[10]);
            $team->setContactNumber($row[11]);
            if (trim($row[8])) {
                $dt = \DateTime::createFromFormat('m/j/Y', trim($row[8]));
                $team->setDateArrived($dt);
            }
            if (trim($row[9])) {
                $dt2 = \DateTime::createFromFormat('m/j/Y', trim($row[9]));
                if ($dt2) {
                    $team->setExpectedDeparture($dt2);
                } else if ($team->getDateArrived ()) {
                    $add = strtotime(trim($row[9]), $team->getDateArrived()->getTimestamp());
                    $dt3 = new \DateTime();
                    $dt3->setTimestamp($add);
                    $team->setExpectedDeparture($dt3);
                }
            }
            $em->persist($team);
        }
        
        $em->flush();
        
        $output->writeln(PHP_EOL.'Done');
    }
    
    private function getTeam($name)
    {
        static $list = array();
        if (!isset($list[$name])) {
            $em = $this->getContainer()->get('doctrine')->getManager();
            $entity = $em->getRepository('RMSRecoveryBundle:HealthTeam')
                    ->findOneBy(array('name' => $name));
            if (!$entity) {
                $entity = new HealthTeam();
                $entity->setName($name);
            }
            $list[$name] = $entity;
        }
        return $list[$name];
    }
    
    private function getLocation($name)
    {
        static $list = array();
        if (!isset($list[$name])) {
            $em = $this->getContainer()->get('doctrine')->getManager();
            $entity = $em->getRepository('RMSRecoveryBundle:HealthLocation')
                    ->findOneBy(array('name' => $name));
            if (!$entity) {
                $entity = new HealthLocation();
                $entity->setName($name);
            }
            $em->persist($entity);
            $list[$name] = $entity;
        }
        return $list[$name];
    }
    
}
