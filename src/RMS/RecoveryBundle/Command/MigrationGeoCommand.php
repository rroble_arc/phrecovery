<?php

namespace RMS\RecoveryBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class MigrationGeoCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('rms:geo:migrate');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $result = $em->getRepository('RMSRecoveryBundle:Geo')->findAll();
        
        foreach ($result as $geo) {
            $geo->setGeoName($geo->getName());
            $em->persist($geo);
        }
        $em->flush();

        $output->writeln(PHP_EOL . 'Done');
    }

}
