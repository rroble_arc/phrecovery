<?php

namespace RMS\RecoveryBundle\Command;

use RMS\RecoveryBundle\Entity\UserAdmin;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class CreateAdminCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('rms:admin:create');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* @var $em ObjectManager */
        $em = $this->getContainer()->get('doctrine')->getManager();

        try {
            $admin = new UserAdmin();
            $admin->setUsername('super_admin');
            $admin->setEmail('super_admin@phrecovery.org');
            $admin->setPassword('Welcome!');
            $admin->setFirstname('Administrator');
            $admin->setLastname('');
            $admin->addRole(UserAdmin::ROLE_SUPER_ADMIN);

            $em->persist($admin);
            $em->flush();

            $output->writeln('Admin has been created successfully!');
        } catch(\Exception $ex) {
            $output->writeln($ex->getMessage());
        }
    }

}
