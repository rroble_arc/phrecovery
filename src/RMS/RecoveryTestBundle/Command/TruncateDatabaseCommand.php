<?php

namespace RMS\RecoveryTestBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class TruncateDatabaseCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
                ->setName('rms:database:truncate')
                ->setDescription('Truncate database')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $output->writeln('Removing existing data...');
        
        $entities = array(
            'RMSRecoveryBundle:Aid',
            'RMSRecoveryBundle:AidNeeded',
            'RMSRecoveryBundle:AidType',
            'RMSRecoveryBundle:Barangay',
            'RMSRecoveryBundle:Municipality',
            'RMSRecoveryBundle:Geo',
            'RMSRecoveryBundle:Ngo',
            'RMSRecoveryBundle:Province',
            'RMSRecoveryBundle:Situation',
            'RMSRecoveryBundle:Status',
            'RMSRecoveryBundle:UserAdmin',
            'RMSRecoveryBundle:UserNgo',
            'RMSRecoveryBundle:User',
        );
        foreach ($entities as $entity) {
            $repo = $em->getRepository($entity);
            $output->writeln($repo->getClassName());
            $rows = $repo->findAll();
            foreach ($rows as $row) {
                $em->remove($row);
            }
            $em->flush();
        }
        $output->writeln('Done');
    }

}
