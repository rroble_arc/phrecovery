<?php

namespace RMS\RecoveryTestBundle\Tests;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class BehatTest extends BehatTestCase
{

    protected function getFeatures()
    {
        return '@RMSRecoveryTestBundle';
    }

    public function testFeatures()
    {
        $result = $this->runBehat();
        $this->assertEquals(0, $result);
    }

}
