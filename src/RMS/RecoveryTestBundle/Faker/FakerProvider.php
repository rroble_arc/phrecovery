<?php

namespace RMS\RecoveryTestBundle\Faker;

use RMS\RecoveryBundle\Entity as E;
use Faker\Generator;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class FakerProvider
{

    /**
     * @var Generator
     */
    protected $generator;

    public function __construct(Generator $generator)
    {
        $this->generator = $generator;
        $generator->addProvider($this);
    }

    public function format($formatter)
    {
        return $this->generator->format($formatter);
    }

    public function uniqueEmail()
    {
        return implode(array(
                    $this->uniqueUsername(),
                    '.', time(), '@',
                    $this->format('domainName')
                ));
    }

    public function uniqueUsername()
    {
        return implode(array(
                    $this->format('userName'),
                    '.', time()
                ));
    }

    public function geo()
    {
        $r = rand(1, 3);
        switch ($r) {
            case 1: $geo = $this->barangay();
                break;
            case 2: $geo = $this->municipality();
                break;
            case 3: $geo = $this->province();
                break;
        }
        return $geo;
    }

    public function barangay($rel = true)
    {
        $barangay = new E\Barangay();
        $barangay->setName($this->format('streetName'));
        $barangay->setPopulation(rand());
        $barangay->setEstimatedHouseholds(rand());
        $barangay->setAffectedHouseholds(rand());
        if ($rel) {
            $barangay->setMunicipality($this->municipality());
            $r = rand(1,3);
            for ($i = 0; $i < $r; $i++) {
                $barangay->addSituation($this->situation());
                $barangay->addNeed($this->aidNeeded());
            }
        }
        return $barangay;
    }
    
    public function aidNeeded()
    {
        $need = new E\AidNeeded();
        $need->setReporter($this->user());
        $need->setDescription($this->format('text'));
        $need->setStatus($this->format('text'));
        return $need;
    }
    
    public function situation()
    {
        $situation = new E\Situation();
        $situation->setStatus($this->status());
        $situation->setReporter($this->user());
        return $situation;
    }

    public function municipality()
    {
        $municipality = new E\Municipality();
        $municipality->setName($this->format('streetName'));
        $municipality->setProvince($this->province());
        $municipality->addBarangay($this->barangay(false));
        return $municipality;
    }

    public function province()
    {
        $province = new E\Province();
        $province->setName($this->format('streetName'));
        return $province;
    }

    public function ngo()
    {
        $ngo = new E\Ngo();
        $ngo->setName($this->format('text'));
        $ngo->setLogo($this->logo());
        $ngo->setDescription($this->format('text'));
        $ngo->setStreet($this->format('streetName'));
        $ngo->setCity($this->format('city'));
        $ngo->setCountry($this->format('country'));
        $ngo->setZipcode($this->format('postcode'));
        $ngo->setStatus(rand());
        return $ngo;
    }

    public function reporter()
    {
        if (rand(1, 2) == 1) {
            return $this->user();
        }
        return $this->userNgo();
    }

    public function user()
    {
        $user = new E\User();
        return $this->setUserDetails($user);
    }
    
    private function setUserDetails(E\User $user)
    {
        $user->setUsername($this->uniqueUsername());
        $user->setPassword(md5(rand()));
        $user->setSalt(md5(rand()));
        $user->setEmail($this->uniqueEmail());
        $user->setFirstname($this->format('firstName'));
        $user->setLastname($this->format('lastName'));
        return $user;
    }

    public function userNgo()
    {
        $user = new E\UserNgo();
        $user->setTitle($this->format('text'));
        $user->setContactNumber($this->format('phoneNumber'));
        $user->setNgo($this->ngo());
        return $this->setUserDetails($user);
    }
    
    public function status()
    {
        $status = new E\Status();
        $status->setDescription($this->format('text'));
        return $status;
    }
    
    public function roleUser()
    {
        return array(E\User::ROLE_DEFAULT);
    }
    
    public function roleNgo()
    {
        return array(E\User::ROLE_NGO);
    }
    
    public function logo()
    {
        $photos = array(
            'http://www.ibiblio.org/solidarity/images/logos/logo-acpc.jpg',
            'http://logofaves.com/wp-content/uploads/2012/09/speak_m.jpg',
            'http://www.hqlogos.com/images/portfolio/non-profit-logo-design.png',
            'http://www.thebusinesslogo.com/logo-design-portfolio/dont_cry_children_tears.gif',
            'http://www.logodesignsmith.com/images/logos/NGO_Logos/4.jpg',
            'http://www.logodesignsmith.com/images/logos/NGO_Logos/7.jpg',
            'http://www.designerstalk.com/forums/attachments/showcase/5274d1217445854-ngo-logo-design-aflorest.jpg',
            'http://www.alliancef.com/images/logos.jpg',
            'http://www.logodesigngenius.com/portfolio/portfolio-logo-design/ngo-logo-design/logo-designs-1.png',
            'http://fc03.deviantart.net/fs71/f/2012/196/1/3/ngo_3d_logo_v2_by_ngo_design-d57cxba.png',
        );
        shuffle($photos);
        return $photos[0];
    }

}
