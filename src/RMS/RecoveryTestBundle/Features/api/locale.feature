Feature: Locale

Scenario: Default locale
    When I go to "/test/locale"
    Then the response should contain "locale=en"

Scenario: French
    When I go to "/test/locale?lang=fr"
    Then the response should contain "locale=fr"

Scenario: German
    When I go to "/test/locale?language=de"
    Then the response should contain "locale=de"
