<?php

namespace RMS\RecoveryTestBundle\Features\Context;

use Behat\Behat\Context\BehatContext;

/**
 * @author Randolph Roble <r.roble@arcanys.com>
 */
class FeatureContext extends BehatContext
{

    public function __construct(array $parameters)
    {
        $this->useContext('api', new ApiContext($parameters));
    }

}
