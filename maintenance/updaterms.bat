@echo off

set PROJHOME=%~dp0..
cd %PROJHOME%
cd

rd /q web > NUL
mklink /j web maintenance

rd /q /s app\cache\prod* > NUL
rd /q /s app\cache\dev* > NUL
rd /q /s app\cache\test* > NUL
cmd /c git pull
cmd /c php app/console doctrine:schema:update --force

rd /q web > NUL
mklink /j web webmain

php app/console assets:install web
